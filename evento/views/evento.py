# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'evento.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(415, 426)
        Dialog.setMinimumSize(QSize(415, 426))
        Dialog.setMaximumSize(QSize(509, 426))
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.matricula = QLabel(self.frame)
        self.matricula.setObjectName(u"matricula")
        sizePolicy1 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.matricula, 1, 1, 1, 1)

        self.servidor = QLabel(self.frame)
        self.servidor.setObjectName(u"servidor")
        sizePolicy1.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.servidor, 0, 1, 1, 1)


        self.verticalLayout_2.addWidget(self.frame)

        self.groupBox_2 = QGroupBox(Dialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout = QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")

        self.verticalLayout.addWidget(self.label_4)

        self.descricao = QLineEdit(self.groupBox_2)
        self.descricao.setObjectName(u"descricao")

        self.verticalLayout.addWidget(self.descricao)

        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.informacoes = QTextEdit(self.groupBox_2)
        self.informacoes.setObjectName(u"informacoes")
        self.informacoes.setTabChangesFocus(True)

        self.verticalLayout.addWidget(self.informacoes)


        self.verticalLayout_2.addWidget(self.groupBox_2)

        self.groupBox = QGroupBox(Dialog)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.gridLayout_3 = QGridLayout(self.groupBox)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy2)

        self.gridLayout_3.addWidget(self.label_5, 1, 0, 1, 1)

        self.data_inicial = QDateEdit(self.groupBox)
        self.data_inicial.setObjectName(u"data_inicial")
        self.data_inicial.setCalendarPopup(True)

        self.gridLayout_3.addWidget(self.data_inicial, 1, 1, 1, 1)

        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        sizePolicy2.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy2)

        self.gridLayout_3.addWidget(self.label_6, 1, 2, 1, 1)

        self.data_final = QDateEdit(self.groupBox)
        self.data_final.setObjectName(u"data_final")
        self.data_final.setCalendarPopup(True)

        self.gridLayout_3.addWidget(self.data_final, 1, 3, 1, 1)

        self.indeterminado = QCheckBox(self.groupBox)
        self.indeterminado.setObjectName(u"indeterminado")

        self.gridLayout_3.addWidget(self.indeterminado, 0, 0, 1, 4)


        self.verticalLayout_2.addWidget(self.groupBox)

        self.excluido = QLabel(Dialog)
        self.excluido.setObjectName(u"excluido")
        self.excluido.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.excluido)

        self.servidor_excluido = QLabel(Dialog)
        self.servidor_excluido.setObjectName(u"servidor_excluido")
        self.servidor_excluido.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.servidor_excluido)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy3)
        self.widget.setLayoutDirection(Qt.RightToLeft)
        self.gridLayout_2 = QGridLayout(self.widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.historico = QPushButton(self.widget)
        self.historico.setObjectName(u"historico")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.historico.sizePolicy().hasHeightForWidth())
        self.historico.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.historico, 1, 2, 1, 1)

        self.salvar = QPushButton(self.widget)
        self.salvar.setObjectName(u"salvar")
        sizePolicy4.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.salvar, 1, 1, 1, 1)

        self.excluir = QPushButton(self.widget)
        self.excluir.setObjectName(u"excluir")
        sizePolicy4.setHeightForWidth(self.excluir.sizePolicy().hasHeightForWidth())
        self.excluir.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.excluir, 1, 3, 1, 1)

        self.fechar = QPushButton(self.widget)
        self.fechar.setObjectName(u"fechar")
        sizePolicy4.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.fechar, 1, 4, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer, 1, 5, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_2, 1, 0, 1, 1)


        self.verticalLayout_2.addWidget(self.widget)

        QWidget.setTabOrder(self.descricao, self.informacoes)
        QWidget.setTabOrder(self.informacoes, self.indeterminado)
        QWidget.setTabOrder(self.indeterminado, self.data_inicial)
        QWidget.setTabOrder(self.data_inicial, self.data_final)
        QWidget.setTabOrder(self.data_final, self.fechar)
        QWidget.setTabOrder(self.fechar, self.excluir)
        QWidget.setTabOrder(self.excluir, self.historico)
        QWidget.setTabOrder(self.historico, self.salvar)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.indeterminado.toggled.connect(self.data_final.setDisabled)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Evento", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"servidor", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Dados", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Descri\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00f5es:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Per\u00edodo", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Data inicial:", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Data final:", None))
        self.indeterminado.setText(QCoreApplication.translate("Dialog", u"Per\u00edodo final indefinido", None))
        self.excluido.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0004;\">Evento exclu\u00eddo</span></p></body></html>", None))
        self.servidor_excluido.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0004;\">Servidor exclu\u00eddo</span></p></body></html>", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
    # retranslateUi

