from peewee import *

from servidor.models import Servidor
from QtUtils.historico.models import Historico


class Evento(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True)
    servidor = ForeignKeyField(
        Servidor,
        related_name='eventos',
        on_delete='CASCADE',
        )
    descricao = TextField()
    informacoes = TextField()
    data_inicial = DateField()
    data_final = DateField(null=True)
    total = IntegerField(null=True)
    indeterminado = BooleanField()

    def __str__(self):
        return str(self.descricao)
    
    def evento_str(self) -> str:
        return '{descricao} - DE {data_i} ATÉ {data_f}{inf}'.format(
            descricao=self.descricao,
            data_i=self.strftime('data_inicial'),
            data_f=self.strftime('data_final'),
            inf=': ' + self.informacoes if self.informacoes else ''
        )
    
    @staticmethod
    def exportar_extras():
        return ['servidor', 'servidor.lotacao', 'servidor.cargo']

    @staticmethod
    def ignorar_campos_mudancas():
        return ['servidor'] + Historico.ignorar_campos_mudancas()
    
    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).join(Servidor).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(cls.data_inicial.desc())