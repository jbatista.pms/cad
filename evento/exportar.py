import csv
from PySide2 import QtWidgets

from QtUtils.colecoes.dialogos import Alerta
from evento.models import Evento
from servidor.models import Servidor

def exportar_eventos(parent, servidores=Servidor.select2()):
    cabecalho = [
        'servidor__nome',
        'servidor__matricula',
        'servidor__cargo',
        'servidor__lotacao',
        'servidor__situacao',
        'evento__descricao',
        'evento__informacoes',
        'evento__data_inicial',
        'evento__data_final',
        'evento__dias',
    ]

    # Selecionar arquivo destino
    arq_nome = QtWidgets.QFileDialog.getSaveFileName(parent,
            "Exportar avaliações", '',
            "CSV com cabeçalho (*.csv);;All Files (*)")
    if not arq_nome[0]:
        return

    # Preparar arquivo
    try:    
        arq = open(arq_nome[0], 'w')
    except PermissionError as a:
        return Alerta(
            parent,
            text=(
                "Não foi possível salvar!\n\n"
                "O arquivo está aberto em outro programa ou sendo utilizado por algum processo.\n"
            )
        ).exec_()
    arq_csv = csv.DictWriter(
        arq, 
        fieldnames=cabecalho, 
        delimiter=';', 
        lineterminator='\n'
    )
    arq_csv.writeheader()

    # Popular linhas
    linhas = []
    for ser in servidores:
        for ev in Evento.select2().filter(Evento.servidor==ser):
            result = {
                'servidor__nome': ser.nome,
                'servidor__matricula': ser.matricula,
                'servidor__cargo': ser.cargo.nome,
                'servidor__lotacao': ser.lotacao.nome,
                'servidor__situacao': ser.situacao.nome,
                'evento__descricao': ev.descricao,
                'evento__informacoes': ev.informacoes,
                'evento__data_inicial': ev.data_inicial,
                'evento__data_final': '' if ev.indeterminado else ev.data_final,
                'evento__dias': '' if ev.indeterminado else ev.total,
            }
            linhas.append(result)

    # Finalizar e salvar arquivo
    arq_csv.writerows(linhas)
    arq.close()