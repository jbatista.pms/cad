import sys
from datetime import date
from PySide2 import QtCore

from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Alerta, Confirmacao
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.historico import historico
from QtUtils.text import text_format_uppercase
from QtUtils.validadores import TextoEmMaisculo

from avaliacao.models import Avaliacao
from .models import Evento
from .views.evento import Ui_Dialog


class Control(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog
    
    def alerta_impedimento(self, avaliacao=None):
        avaliacao = avaliacao or self.avaliacao_coincidente()
        if avaliacao and (avaliacao.ficha_enviada or avaliacao.ficha_devolvida):
            titulo = "Impossível alterar o registro"
            mensagem = (
                "Período concide com avaliação concluída ou em andamento."
            )
            informacao = (
                "Verifique entre as fichas de acompanhamento e os formulários de "
                "avaliação, aqueles que correspondem ao período "
                "compreendido no evento."
            )
            alrt = Alerta(self, text=mensagem, title=titulo)
            alrt.setInformativeText(informacao)
            alrt.exec_()
            return True
        return False

    def avaliacao_coincidente(self, data_inicial=None, data_final=None):
        data_inicial = data_inicial or self.instance.data_inicial
        data_final = data_inicial or self.instance.data_final
        avaliacoes = self.instance.servidor.avaliacoes.select().where(
            (Avaliacao.data_final>=data_inicial) &
            (Avaliacao.data_inicial<=data_inicial)
            )
        return avaliacoes[0] if avaliacoes else None

    def excluir(self):
        if self.instance.id != None and not self.alerta_impedimento():
            if Confirmacao(self, text="Excluir evento?").exec_():
                self.instance.excluir_instancia()
                self.enviar_sinal_atualizacao(self.instance)
                self.accept()

    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        self.ui.servidor.setText(self.instance.servidor.nome)
        self.ui.matricula.setText(self.instance.servidor.matricula)
        self.ui.descricao.setValidator(TextoEmMaisculo(self))
        text_format_uppercase(self.ui.informacoes)
        if self.instance.id:
            self.ui.descricao.setText(self.instance.descricao)
            self.ui.informacoes.setPlainText(self.instance.informacoes)
            self.ui.data_inicial.setDate(
                datetime_qdate(self.instance.data_inicial)
                )
            self.ui.data_final.setDate(
                datetime_qdate(self.instance.data_final)
                )
            if self.instance.indeterminado:
                self.ui.indeterminado.setCheckState(QtCore.Qt.Checked)
            else:
                self.ui.indeterminado.setCheckState(QtCore.Qt.Unchecked)
            if self.instance.data_exclusao is None:
                self.ui.excluido.hide()
                if self.instance.servidor.data_exclusao is None:
                    self.ui.servidor_excluido.hide()
                else:
                    self.ui.servidor_excluido.show()
                    self.ui.excluir.setEnabled(False)
                    self.ui.salvar.setEnabled(False)
            else:
                self.ui.servidor_excluido.hide()
                self.ui.excluido.show()
                self.ui.excluir.setEnabled(False)
                self.ui.salvar.setEnabled(False)

        else:
            data_hoje = datetime_qdate(date.today())
            self.ui.excluido.hide()
            self.ui.excluir.hide()
            self.setWindowTitle("Novo evento")
            self.ui.data_inicial.setDate(data_hoje)
            self.ui.data_final.setDate(data_hoje)
            self.ui.historico.hide()
            self.ui.servidor_excluido.hide()

    def salvar(self):
        data_inicial = qdate_datetime(self.ui.data_inicial.date())
        data_final = qdate_datetime(self.ui.data_final.date())
        indeterminado = self.ui.indeterminado.isChecked()
        avaliacao = self.avaliacao_coincidente(data_inicial, data_final)
        descricao = self.ui.descricao.text()

        if not descricao:
            return Alerta(
                parent=self,
                text="É necessário uma descrição para o evento.",
                title="Não foi possível salvar",
            ).exec_()
        elif data_inicial > data_final and not indeterminado:
            return Alerta(
                parent=self,
                text="Data inicial é maior que data final!",
                title="Não foi possível salvar"
            ).exec_()
        elif not avaliacao:
            return Alerta(
                parent=self,
                text="Período não coincide com nenhuma avaliação.",
                title="Não foi possível salvar"
            ).exec_()
        elif self.alerta_impedimento(avaliacao):
            return
        elif Evento.select2().where(
                (Evento.servidor==self.instance.servidor) &
                (Evento.id!=self.instance.id) &
                ((
                    (Evento.data_inicial<=data_inicial) & 
                    (Evento.data_final>=data_inicial)
                ) | (
                    (Evento.data_inicial<=data_final) &
                    (Evento.data_final>=data_final)
                ))
            ).count():

            return Alerta(
                parent=self,
                text="Existe evento com período concidente. Verifique.",
                title="Não foi possível salvar"
            ).exec_()

        self.instance.informacoes = self.ui.informacoes.toPlainText()
        self.instance.descricao = descricao
        self.instance.indeterminado = indeterminado
        self.instance.data_inicial = data_inicial
        self.instance.data_final = data_final
        self.instance.total = (
            self.instance.data_final - self.instance.data_inicial
            ).days + 1
        self.instance.save()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False
