from QtUtils.relatorio.dados import ModeloRelatorio
from .models import Evento


class EventoModeloRelatorio(ModeloRelatorio):
    modelo = Evento
    funcoes = {
        'data_final': lambda ev: 'Indeterminado' if ev.indeterminado else ev.data_final,
    }