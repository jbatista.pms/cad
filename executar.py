from QtUtils import Definicoes, QtUtils

definicoes_cliente = {
    'abreviatura_nome': 'CAD',
    'classe_janela_principal': 'app.JanelaPrincipal',
    'inicializar_cef': True,
    'nome_aplicacao': 'Comissão para Avaliacao de Desempenho do Servidor em Estágio Probatório - CAD',
    'modelos': [
        'ComumComissoes.cargo.models.Cargo',
        'ComumComissoes.lotacao.models.Lotacao',
        'situacao.models.Situacao',
        'servidor.models.Servidor',
        'servidor.models.Informacao',
        'servidor.models.LocalDeTrabalho',
        'avaliacao.models.Avaliacao',
        'evento.models.Evento',
    ],
    'modo_execucao': Definicoes.MODO_REDE,
    'verificar_atualizacao': True,
}

if __name__ == '__main__':
    if QtUtils.configurar(definicoes_cliente):
        QtUtils.executar()