from PySide2 import QtCore

from QtUtils import subwindowsbase
from QtUtils.validadores import TextoEmMaisculo
from QtUtils.qt import QTableWidgetItem

from . import servidor
from .cadastro import Cadastro
from .models import Servidor
from .views.listar import *


class ServidoresListaJanela(subwindowsbase.Listar):
    classe_ui = Ui_Form
    lotacao = None

    def inicializar(self, *args, **kwargs):
        self.ui.pesquisa.setValidator(TextoEmMaisculo(self))
        self.ui.pesquisa.setFocus()
        self.atualizar()

    def atualizar(self, *args, **kwargs):
        pesquisa = self.ui.pesquisa.text()

        servidores = Servidor.select2(excluidos=self.ui.excluido.isChecked())
        if pesquisa != '':
            servidores = servidores.where(
                Servidor.nome.contains(pesquisa) |
                Servidor.matricula.contains(pesquisa)
            )

        # Quantidade 
        quantidade = servidores.count()
        if quantidade == 1:
            self.ui.contador.setText("%i registro econtrado." % quantidade)
        else:
            self.ui.contador.setText("%i registros econtrados." % quantidade)

        # Popular tabela
        self.ui.tabela.setRowCount(quantidade)
        self.lista_dict = {str(s.id):s for s in servidores.objects()}
        for i,ser in enumerate(self.lista_dict.values()):
            # Id
            item = QTableWidgetItem()
            item.setText(str(ser.id))
            self.ui.tabela.setVerticalHeaderItem(i, item)
            # Matrícula
            item = QTableWidgetItem()
            item.setText(ser.matricula)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.tabela.setItem(i, 0, item)
            # Nome
            item = QTableWidgetItem()
            item.setText(ser.nome)
            self.ui.tabela.setItem(i, 1, item)

    def abrir(self, row, column):
        instance = self.lista_dict[self.ui.tabela.verticalHeaderItem(row).text()]
        servidor.Control(self, instance=instance).show()

    def cadastrar(self):
        Cadastro(self).exec_()

    def receber_sinal_atualizacao(self, instance):
        if type(instance) == Servidor:
            self.atualizar()

    def receber_sinal_exclusao(self, instance):
        if type(instance) == Servidor:
            self.atualizar()
