from QtUtils import subwindowsbase
from QtUtils.colecoes import dialogos
from QtUtils.historico.historico import Historico
from QtUtils.text import text_format_uppercase
from QtUtils.validadores import TextoEmMaisculo

from .views import informacao


class InformacaoFormulario(subwindowsbase.Formulario):
    classe_ui = informacao.Ui_Dialog
    
    def accept(self):
        self.instance.titulo = self.ui.titulo.text().upper()
        self.instance.informacao = self.ui.informacao.toPlainText().upper()
        if self.instance.informacao and self.instance.titulo:
            self.instance.save()
            return super().accept()
        dialogos.Informacao(self, text='Todos os campos devem ser preenchidos.').exec_()
    
    def excluir(self):
        if dialogos.Confirmacao(self, text="Excluir informação?").exec_():
            self.instance.excluir_instancia()
            self.enviar_sinal_atualizacao(self.instance)
            return self.accept()
    
    def historico(self):
        Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        text_format_uppercase(self.ui.informacao)
        self.ui.titulo.setValidator(TextoEmMaisculo(self))
        if self.instance.id:
            self.ui.titulo.setText(self.instance.titulo)
            self.ui.informacao.setPlainText(self.instance.informacao)
            if self.instance.data_exclusao or self.instance.servidor.data_exclusao:
                self.ui.excluir.setEnabled(False)
                self.ui.salvar.setEnabled(False)
            if self.instance.data_exclusao is None:
                self.ui.excluido.hide()
        else:
            self.setWindowTitle('Nova informação')
            self.ui.excluir.hide()
            self.ui.excluido.hide()
            self.ui.historico.hide()
