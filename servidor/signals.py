from playhouse.signals import post_save
from QtUtils.db import DataBase

from .models import LocalDeTrabalho, Servidor

@post_save(sender=Servidor)
def criar_local(model_class, instance, created):
    if created:
        LocalDeTrabalho.create(
            servidor=instance,
            lotacao=instance.lotacao,
            data_inicio=instance.data_admissao,
        )
    elif instance.local.lotacao != instance.lotacao:
        LocalDeTrabalho(
            servidor=instance,
            lotacao=instance.lotacao,
            data_inicio=DataBase().data(),
        ).save()

@post_save(sender=LocalDeTrabalho)
def atualizar_lotacao_servidor(model_class, instance, created):
    if created:
        if instance.servidor.lotacao != instance.lotacao:
            servidor = instance.servidor
            servidor.lotacao = instance.lotacao
            servidor.save()
    else:
        local_atual = instance.servidor.local
        if local_atual.lotacao != instance.lotacao:
            servidor = instance.servidor
            servidor.lotacao = local_atual.lotacao
            servidor.save()