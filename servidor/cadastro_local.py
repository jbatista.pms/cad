from datetime import date

from ComumComissoes.lotacao.controles_uteis import LotacaoSelecao
from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Alerta, Confirmacao
from QtUtils.date import datetime_qdate, qdate_datetime
from QtUtils.db import DataBase
from QtUtils.historico import historico

from avaliacao.models import Avaliacao
from avaliacao.utils import estimar_datas_servidor
from .views.cadastro_local import Ui_Dialog


class CadastroLocalDeTrabalho(subwindowsbase.Formulario, LotacaoSelecao):
    classe_ui = Ui_Dialog
    selecao_busca = False
        
    def excluir(self):
        if self.teste():
            return Alerta(
                parent=self,
                text="Não é possível a exclusão.\nExistem formulários já encaminhados para este local.",
                title="Não foi possível excluir!",
            ).exec_()
        elif Confirmacao(
                    parent=self,
                    text="Confirma a exclusão do local de trabalho?",
                    title="Excluir?",
                ).exec_():
            try:
                with DataBase().obter_database().atomic():
                    self.instance.excluir_instancia()
                    estimar_datas_servidor(self.instance.servidor)
                    self.enviar_sinal_atualizacao(self.instance)
                    self.enviar_sinal_atualizacao(self.instance.servidor)
                    self.accept()
            except Exception as e:
                Alerta(
                    parent=self,
                    text="Houve um erro ao processar a requisição.\nPor favor verifique!\n\n%s" % str(e),
                    title="Erro no processo!",
                ).exec_()

    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        self.carregar_lotacoes()
        self.ui.informacao.hide()
        if self.instance.id:
            self.ui.data_inicio.setDate(
                datetime_qdate(self.instance.data_inicio)
            )
            self.selecionar_lotacao(self.instance.lotacao)
            informacao = ''
            if self.instance.data_exclusao:
                self.ui.informacao.show()
                self.ui.excluir.setEnabled(False)
                self.ui.salvar.setEnabled(False)
                informacao += "Local de trabalho excluído."
            if self.instance.servidor.data_exclusao:
                informacao += ' ' if informacao else ''
                informacao += "Servidor excluído."
            if informacao:
                self.ui.informacao.show()
                self.ui.informacao.setText(
                    '<span style=" color:#ff0004;">%s</span>' % informacao
                )
        else:
            self.ui.excluir.hide()
            self.ui.historico.hide()
            self.ui.data_inicio.setDate(
                datetime_qdate(date.today())
            )

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance')

    def salvar(self):
        lotacao = self.locatao_resultado()
        data_inicio = qdate_datetime(self.ui.data_inicio.date())
        if lotacao and data_inicio:
            if self.instance.servidor.data_admissao > data_inicio:
                return Alerta(
                    parent=self,
                    text=(
                        "Não é possível concluir a ação.\n"
                        "A data de início e anterior a admissão do servidor."
                    ),
                    title="Não é possível concluir!",
                ).exec_()
            elif self.instance.id and self.teste():
                return Alerta(
                    parent=self,
                    text="Não é possível alterar o registro.\nExistem formulários já encaminhados para este local.",
                    title="Não é possível concluir!",
                ).exec_()
            local_atual = self.instance.servidor.lotacao_na_data(data_inicio)
            if local_atual.data_inicio == data_inicio and local_atual.id != self.instance.id:
                return Alerta(
                    parent=self,
                    text=(
                        "Não é possível concluir a ação.\n"
                        "Existem locais de trabalho que coincidem com a data de início inserida."
                    ),
                    title="Não é possível concluir!",
                ).exec_()
            with DataBase().obter_database().atomic():
                self.instance.lotacao = lotacao
                self.instance.data_inicio = data_inicio 
                self.instance.save()
                estimar_datas_servidor(self.instance.servidor)
                self.enviar_sinal_atualizacao(self.instance)
                self.enviar_sinal_atualizacao(self.instance.servidor)
                self.accept()
        else:
            return Alerta(
                parent=self,
                text="Os campos lotação e data de início precisam ser preenchidos.",
                title="Não foi possível salvar",
            ).exec_()
    
    def teste(self):
        return Avaliacao.select2().filter((Avaliacao.local==self.instance)&(Avaliacao.ficha_enviada==True)).count()