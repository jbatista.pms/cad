# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(427, 458)
        Form.setMinimumSize(QSize(427, 458))
        self.horizontalLayout_2 = QHBoxLayout(Form)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.frame_2 = QFrame(Form)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_3 = QFrame(self.frame_2)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_3)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.pesquisa = QLineEdit(self.frame_3)
        self.pesquisa.setObjectName(u"pesquisa")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pesquisa.sizePolicy().hasHeightForWidth())
        self.pesquisa.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.pesquisa)

        self.pushButton = QPushButton(self.frame_3)
        self.pushButton.setObjectName(u"pushButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy1)
        self.pushButton.setMaximumSize(QSize(22, 22))
        self.pushButton.setLayoutDirection(Qt.LeftToRight)

        self.horizontalLayout.addWidget(self.pushButton)

        self.excluido = QCheckBox(self.frame_3)
        self.excluido.setObjectName(u"excluido")

        self.horizontalLayout.addWidget(self.excluido)


        self.verticalLayout_2.addWidget(self.frame_3)

        self.tabela = QTableWidget(self.frame_2)
        if (self.tabela.columnCount() < 2):
            self.tabela.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        if (self.tabela.rowCount() < 1):
            self.tabela.setRowCount(1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tabela.setVerticalHeaderItem(0, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tabela.setItem(0, 0, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tabela.setItem(0, 1, __qtablewidgetitem4)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setFrameShape(QFrame.StyledPanel)
        self.tabela.setFrameShadow(QFrame.Sunken)
        self.tabela.setAutoScroll(True)
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setTabKeyNavigation(True)
        self.tabela.setDragEnabled(False)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setGridStyle(Qt.CustomDashLine)
        self.tabela.setSortingEnabled(False)
        self.tabela.setWordWrap(True)
        self.tabela.setCornerButtonEnabled(True)
        self.tabela.horizontalHeader().setCascadingSectionResizes(True)
        self.tabela.horizontalHeader().setMinimumSectionSize(30)
        self.tabela.horizontalHeader().setDefaultSectionSize(100)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.horizontalHeader().setProperty("showSortIndicator", True)
        self.tabela.horizontalHeader().setStretchLastSection(True)
        self.tabela.verticalHeader().setVisible(False)
        self.tabela.verticalHeader().setCascadingSectionResizes(False)

        self.verticalLayout_2.addWidget(self.tabela)

        self.contador = QLabel(self.frame_2)
        self.contador.setObjectName(u"contador")

        self.verticalLayout_2.addWidget(self.contador)


        self.horizontalLayout_2.addWidget(self.frame_2)

        QWidget.setTabOrder(self.pesquisa, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.excluido)
        QWidget.setTabOrder(self.excluido, self.tabela)

        self.retranslateUi(Form)
        self.tabela.cellDoubleClicked.connect(Form.abrir)
        self.pushButton.clicked.connect(self.pesquisa.clear)
        self.pesquisa.textChanged.connect(Form.atualizar)
        self.pushButton.clicked.connect(self.pesquisa.setFocus)
        self.excluido.stateChanged.connect(Form.atualizar)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Servidores", None))
        self.pesquisa.setPlaceholderText(QCoreApplication.translate("Form", u"Nome ou matr\u00edcula", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"X", None))
        self.excluido.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Nome", None));
        ___qtablewidgetitem2 = self.tabela.verticalHeaderItem(0)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Teste", None));

        __sortingEnabled = self.tabela.isSortingEnabled()
        self.tabela.setSortingEnabled(False)
        ___qtablewidgetitem3 = self.tabela.item(0, 0)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"10/2231-07", None));
        ___qtablewidgetitem4 = self.tabela.item(0, 1)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Form", u"JO\u00c3O BATISTA DOS SANTOS FILHO", None));
        self.tabela.setSortingEnabled(__sortingEnabled)

        self.contador.setText(QCoreApplication.translate("Form", u"0 registros econtrados.", None))
    # retranslateUi

