# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'alterar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(447, 206)
        Dialog.setMinimumSize(QSize(0, 0))
        Dialog.setMaximumSize(QSize(447, 206))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.nome = QLineEdit(self.frame)
        self.nome.setObjectName(u"nome")

        self.gridLayout_2.addWidget(self.nome, 0, 1, 1, 3)

        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_3, 1, 2, 1, 1)

        self.matricula = QLineEdit(self.frame)
        self.matricula.setObjectName(u"matricula")

        self.gridLayout_2.addWidget(self.matricula, 1, 1, 1, 1)

        self.label_4 = QLabel(self.frame)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_4, 2, 0, 1, 1)

        self.cargo = QComboBox(self.frame)
        self.cargo.setObjectName(u"cargo")

        self.gridLayout_2.addWidget(self.cargo, 2, 1, 1, 3)

        self.data_admissao = QDateEdit(self.frame)
        self.data_admissao.setObjectName(u"data_admissao")
        self.data_admissao.setCalendarPopup(True)

        self.gridLayout_2.addWidget(self.data_admissao, 1, 3, 1, 1)

        self.label_2 = QLabel(self.frame)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_2, 1, 0, 1, 1)

        self.label_6 = QLabel(self.frame)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_2.addWidget(self.label_6, 4, 0, 1, 1)

        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.label_5 = QLabel(self.frame)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_5, 3, 0, 1, 1)

        self.lotacao = QComboBox(self.frame)
        self.lotacao.setObjectName(u"lotacao")

        self.gridLayout_2.addWidget(self.lotacao, 3, 1, 1, 3)

        self.sublotacao = QComboBox(self.frame)
        self.sublotacao.setObjectName(u"sublotacao")

        self.gridLayout_2.addWidget(self.sublotacao, 4, 1, 1, 3)

        self.label_7 = QLabel(self.frame)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_7, 5, 0, 1, 1)

        self.situacao = QComboBox(self.frame)
        self.situacao.setObjectName(u"situacao")

        self.gridLayout_2.addWidget(self.situacao, 5, 1, 1, 3)


        self.gridLayout.addWidget(self.frame, 0, 0, 1, 2)

        self.frame1 = QFrame(Dialog)
        self.frame1.setObjectName(u"frame1")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame1.sizePolicy().hasHeightForWidth())
        self.frame1.setSizePolicy(sizePolicy)
        self.frame1.setFrameShape(QFrame.StyledPanel)
        self.frame1.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame1)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.frame1)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.excluir = QPushButton(self.frame1)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.pushButton = QPushButton(self.frame1)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.gridLayout.addWidget(self.frame1, 8, 0, 1, 2)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 7, 0, 1, 2)

        QWidget.setTabOrder(self.nome, self.matricula)
        QWidget.setTabOrder(self.matricula, self.data_admissao)
        QWidget.setTabOrder(self.data_admissao, self.cargo)
        QWidget.setTabOrder(self.cargo, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.excluir)
        QWidget.setTabOrder(self.excluir, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)
        self.excluir.released.connect(Dialog.excluir)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de Servidor", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Data de admiss\u00e3o:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Cargo:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Sublota\u00e7\u00e3o:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Situa\u00e7\u00e3o:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

