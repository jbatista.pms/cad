# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'relatorio.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(777, 458)
        Form.setMinimumSize(QSize(777, 458))
        self.horizontalLayout_2 = QHBoxLayout(Form)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setMaximumSize(QSize(250, 16777215))
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.tabWidget = QTabWidget(self.frame)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout = QGridLayout(self.tab)
        self.gridLayout.setObjectName(u"gridLayout")
        self.situacao = QComboBox(self.tab)
        self.situacao.setObjectName(u"situacao")

        self.gridLayout.addWidget(self.situacao, 7, 0, 1, 1)

        self.label_3 = QLabel(self.tab)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 4, 0, 1, 1)

        self.contador = QLabel(self.tab)
        self.contador.setObjectName(u"contador")

        self.gridLayout.addWidget(self.contador, 10, 0, 1, 1)

        self.label_4 = QLabel(self.tab)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_4, 0, 0, 1, 1)

        self.lotacao = QComboBox(self.tab)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.lotacao, 3, 0, 1, 1)

        self.cargo = QComboBox(self.tab)
        self.cargo.setObjectName(u"cargo")
        sizePolicy.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.cargo, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 8, 0, 1, 1)

        self.pushButton_2 = QPushButton(self.tab)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setMaximumSize(QSize(22, 22))

        self.gridLayout.addWidget(self.pushButton_2, 1, 1, 1, 1)

        self.pushButton_6 = QPushButton(self.tab)
        self.pushButton_6.setObjectName(u"pushButton_6")

        self.gridLayout.addWidget(self.pushButton_6, 9, 0, 1, 2)

        self.pushButton_5 = QPushButton(self.tab)
        self.pushButton_5.setObjectName(u"pushButton_5")
        self.pushButton_5.setMaximumSize(QSize(22, 22))

        self.gridLayout.addWidget(self.pushButton_5, 7, 1, 1, 1)

        self.pushButton_3 = QPushButton(self.tab)
        self.pushButton_3.setObjectName(u"pushButton_3")
        self.pushButton_3.setMaximumSize(QSize(22, 22))

        self.gridLayout.addWidget(self.pushButton_3, 3, 1, 1, 1)

        self.sublotacao = QComboBox(self.tab)
        self.sublotacao.setObjectName(u"sublotacao")

        self.gridLayout.addWidget(self.sublotacao, 5, 0, 1, 1)

        self.pushButton_4 = QPushButton(self.tab)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setMaximumSize(QSize(22, 22))

        self.gridLayout.addWidget(self.pushButton_4, 5, 1, 1, 1)

        self.label_5 = QLabel(self.tab)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 6, 0, 1, 1)

        self.label_2 = QLabel(self.tab)
        self.label_2.setObjectName(u"label_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy1)
        self.label_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_3 = QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.pushButton = QPushButton(self.tab_2)
        self.pushButton.setObjectName(u"pushButton")

        self.verticalLayout_3.addWidget(self.pushButton)

        self.label = QLabel(self.tab_2)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label.setWordWrap(True)

        self.verticalLayout_3.addWidget(self.label)

        self.pushButton_7 = QPushButton(self.tab_2)
        self.pushButton_7.setObjectName(u"pushButton_7")

        self.verticalLayout_3.addWidget(self.pushButton_7)

        self.label_6 = QLabel(self.tab_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label_6.setWordWrap(True)

        self.verticalLayout_3.addWidget(self.label_6)

        self.pushButton_8 = QPushButton(self.tab_2)
        self.pushButton_8.setObjectName(u"pushButton_8")

        self.verticalLayout_3.addWidget(self.pushButton_8)

        self.label_7 = QLabel(self.tab_2)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label_7.setWordWrap(True)

        self.verticalLayout_3.addWidget(self.label_7)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.tabWidget.addTab(self.tab_2, "")

        self.verticalLayout.addWidget(self.tabWidget)


        self.horizontalLayout_2.addWidget(self.frame)

        self.frame_2 = QFrame(Form)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.frame_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.tabela = QTableWidget(self.frame_2)
        if (self.tabela.columnCount() < 4):
            self.tabela.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setFrameShape(QFrame.StyledPanel)
        self.tabela.setFrameShadow(QFrame.Sunken)
        self.tabela.setAutoScroll(True)
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setTabKeyNavigation(True)
        self.tabela.setDragEnabled(False)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setGridStyle(Qt.CustomDashLine)
        self.tabela.setSortingEnabled(False)
        self.tabela.setWordWrap(True)
        self.tabela.setCornerButtonEnabled(True)
        self.tabela.horizontalHeader().setCascadingSectionResizes(True)
        self.tabela.horizontalHeader().setMinimumSectionSize(30)
        self.tabela.horizontalHeader().setDefaultSectionSize(100)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.horizontalHeader().setProperty("showSortIndicator", True)
        self.tabela.verticalHeader().setVisible(False)
        self.tabela.verticalHeader().setCascadingSectionResizes(False)

        self.verticalLayout_2.addWidget(self.tabela)


        self.horizontalLayout_2.addWidget(self.frame_2)

        QWidget.setTabOrder(self.tabWidget, self.tabela)
        QWidget.setTabOrder(self.tabela, self.cargo)
        QWidget.setTabOrder(self.cargo, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.situacao)
        QWidget.setTabOrder(self.situacao, self.pushButton_5)
        QWidget.setTabOrder(self.pushButton_5, self.pushButton_6)
        QWidget.setTabOrder(self.pushButton_6, self.pushButton)

        self.retranslateUi(Form)
        self.tabela.cellDoubleClicked.connect(Form.abrir)
        self.pushButton_2.clicked.connect(Form.limpar_cargo)
        self.pushButton_3.clicked.connect(Form.limpar_lotacao)
        self.pushButton_4.clicked.connect(Form.limpar_sublotacao)
        self.pushButton_5.pressed.connect(Form.limpar_situacao)
        self.pushButton_6.pressed.connect(Form.atualizar)
        self.pushButton.clicked.connect(Form.impressao_todos)
        self.pushButton_7.clicked.connect(Form.impressao_por_lotacao)
        self.pushButton_8.clicked.connect(Form.impressao_por_sublotacao)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Servidores", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o:", None))
        self.contador.setText(QCoreApplication.translate("Form", u"0 registros encontrados", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Cargo:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"X", None))
        self.pushButton_6.setText(QCoreApplication.translate("Form", u"Pesquisar", None))
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"X", None))
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"X", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"X", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Situa\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Form", u"Filtros", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.label.setText(QCoreApplication.translate("Form", u"Impress\u00e3o de rela\u00e7\u00e3o de todos os servidores filtrados", None))
        self.pushButton_7.setText(QCoreApplication.translate("Form", u"Por lota\u00e7\u00e3o", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Impress\u00e3o de rela\u00e7\u00e3o de servidores listados, agrupando-os em suas respectivas lota\u00e7\u00f5es.", None))
        self.pushButton_8.setText(QCoreApplication.translate("Form", u"Por sublota\u00e7\u00e3o", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Impress\u00e3o de rela\u00e7\u00e3o de servidores listados, agrupando-os em seus respectivos locais de trabalho.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Impress\u00e3o", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Nome", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem2 = self.tabela.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Cargo", None));
        ___qtablewidgetitem3 = self.tabela.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None));
    # retranslateUi

