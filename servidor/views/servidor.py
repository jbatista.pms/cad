# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'servidor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(672, 481)
        Form.setMinimumSize(QSize(672, 481))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 0, -1, -1)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.alterar = QPushButton(Form)
        self.alterar.setObjectName(u"alterar")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.alterar.sizePolicy().hasHeightForWidth())
        self.alterar.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.alterar)

        self.pushButton_3 = QPushButton(Form)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout.addWidget(self.pushButton_3)

        self.pushButton_4 = QPushButton(Form)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout.addWidget(self.pushButton_4)

        self.pushButton = QPushButton(Form)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_5 = QPushButton(Form)
        self.pushButton_5.setObjectName(u"pushButton_5")

        self.horizontalLayout.addWidget(self.pushButton_5)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout.addLayout(self.horizontalLayout, 3, 2, 1, 2)

        self.excluido = QLabel(Form)
        self.excluido.setObjectName(u"excluido")
        self.excluido.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.excluido, 2, 2, 1, 2)

        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(-1, -1, 0, -1)
        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy1)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label, 3, 1, 1, 1)

        self.cargo = QLabel(Form)
        self.cargo.setObjectName(u"cargo")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy2)

        self.gridLayout_4.addWidget(self.cargo, 3, 2, 1, 3)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        sizePolicy1.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy1)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_2, 1, 1, 1, 1)

        self.nome = QLabel(Form)
        self.nome.setObjectName(u"nome")
        sizePolicy2.setHeightForWidth(self.nome.sizePolicy().hasHeightForWidth())
        self.nome.setSizePolicy(sizePolicy2)

        self.gridLayout_4.addWidget(self.nome, 1, 2, 1, 3)

        self.label_7 = QLabel(Form)
        self.label_7.setObjectName(u"label_7")
        sizePolicy1.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy1)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_7, 2, 1, 1, 1)

        self.matricula = QLabel(Form)
        self.matricula.setObjectName(u"matricula")
        sizePolicy2.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy2)

        self.gridLayout_4.addWidget(self.matricula, 2, 2, 1, 1)

        self.label_5 = QLabel(Form)
        self.label_5.setObjectName(u"label_5")
        sizePolicy1.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy1)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_5, 2, 3, 1, 1)

        self.dataadmissao = QLabel(Form)
        self.dataadmissao.setObjectName(u"dataadmissao")
        sizePolicy2.setHeightForWidth(self.dataadmissao.sizePolicy().hasHeightForWidth())
        self.dataadmissao.setSizePolicy(sizePolicy2)

        self.gridLayout_4.addWidget(self.dataadmissao, 2, 4, 1, 1)

        self.label_3 = QLabel(Form)
        self.label_3.setObjectName(u"label_3")
        sizePolicy1.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy1)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_3, 4, 1, 1, 1)

        self.lotacao = QLabel(Form)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy2.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy2)

        self.gridLayout_4.addWidget(self.lotacao, 4, 2, 1, 3)


        self.gridLayout.addLayout(self.gridLayout_4, 0, 2, 1, 2)

        self.tabWidget = QTabWidget(Form)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tabWidget.setTabShape(QTabWidget.Rounded)
        self.tab_avaliacoes = QWidget()
        self.tab_avaliacoes.setObjectName(u"tab_avaliacoes")
        self.gridLayout_2 = QGridLayout(self.tab_avaliacoes)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.avaliacoes = QTableWidget(self.tab_avaliacoes)
        if (self.avaliacoes.columnCount() < 4):
            self.avaliacoes.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.avaliacoes.rowCount() < 2):
            self.avaliacoes.setRowCount(2)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.avaliacoes.setVerticalHeaderItem(0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.avaliacoes.setVerticalHeaderItem(1, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        __qtablewidgetitem6.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.avaliacoes.setItem(0, 0, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        __qtablewidgetitem7.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.avaliacoes.setItem(0, 1, __qtablewidgetitem7)
        __qtablewidgetitem8 = QTableWidgetItem()
        __qtablewidgetitem8.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.avaliacoes.setItem(0, 2, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        self.avaliacoes.setItem(0, 3, __qtablewidgetitem9)
        self.avaliacoes.setObjectName(u"avaliacoes")
        self.avaliacoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.avaliacoes.setTabKeyNavigation(True)
        self.avaliacoes.setDragEnabled(False)
        self.avaliacoes.setDragDropOverwriteMode(True)
        self.avaliacoes.setDragDropMode(QAbstractItemView.NoDragDrop)
        self.avaliacoes.setAlternatingRowColors(True)
        self.avaliacoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.avaliacoes.setTextElideMode(Qt.ElideNone)
        self.avaliacoes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setGridStyle(Qt.SolidLine)
        self.avaliacoes.setSortingEnabled(False)
        self.avaliacoes.setWordWrap(True)
        self.avaliacoes.setCornerButtonEnabled(True)
        self.avaliacoes.horizontalHeader().setCascadingSectionResizes(True)
        self.avaliacoes.horizontalHeader().setMinimumSectionSize(115)
        self.avaliacoes.horizontalHeader().setDefaultSectionSize(115)
        self.avaliacoes.horizontalHeader().setHighlightSections(False)
        self.avaliacoes.horizontalHeader().setProperty("showSortIndicator", False)
        self.avaliacoes.horizontalHeader().setStretchLastSection(False)
        self.avaliacoes.verticalHeader().setVisible(False)
        self.avaliacoes.verticalHeader().setStretchLastSection(False)

        self.gridLayout_2.addWidget(self.avaliacoes, 0, 0, 1, 2)

        self.tabWidget.addTab(self.tab_avaliacoes, "")
        self.tab_eventos = QWidget()
        self.tab_eventos.setObjectName(u"tab_eventos")
        self.gridLayout_5 = QGridLayout(self.tab_eventos)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.novo_evento = QPushButton(self.tab_eventos)
        self.novo_evento.setObjectName(u"novo_evento")
        sizePolicy.setHeightForWidth(self.novo_evento.sizePolicy().hasHeightForWidth())
        self.novo_evento.setSizePolicy(sizePolicy)

        self.gridLayout_5.addWidget(self.novo_evento, 0, 0, 1, 1)

        self.eventos = QTableWidget(self.tab_eventos)
        if (self.eventos.columnCount() < 4):
            self.eventos.setColumnCount(4)
        __qtablewidgetitem10 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(0, __qtablewidgetitem10)
        __qtablewidgetitem11 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(1, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(2, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(3, __qtablewidgetitem13)
        if (self.eventos.rowCount() < 2):
            self.eventos.setRowCount(2)
        __qtablewidgetitem14 = QTableWidgetItem()
        self.eventos.setVerticalHeaderItem(0, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        self.eventos.setVerticalHeaderItem(1, __qtablewidgetitem15)
        self.eventos.setObjectName(u"eventos")
        self.eventos.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.eventos.setAlternatingRowColors(True)
        self.eventos.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.eventos.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.eventos.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.eventos.horizontalHeader().setCascadingSectionResizes(True)
        self.eventos.horizontalHeader().setMinimumSectionSize(115)
        self.eventos.horizontalHeader().setDefaultSectionSize(115)
        self.eventos.horizontalHeader().setHighlightSections(False)
        self.eventos.horizontalHeader().setProperty("showSortIndicator", True)
        self.eventos.horizontalHeader().setStretchLastSection(False)
        self.eventos.verticalHeader().setVisible(False)

        self.gridLayout_5.addWidget(self.eventos, 2, 0, 1, 3)

        self.excluidos = QCheckBox(self.tab_eventos)
        self.excluidos.setObjectName(u"excluidos")

        self.gridLayout_5.addWidget(self.excluidos, 0, 2, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_5.addItem(self.horizontalSpacer_3, 0, 1, 1, 1)

        self.tabWidget.addTab(self.tab_eventos, "")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_7 = QGridLayout(self.tab)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.nova_informacao = QPushButton(self.tab)
        self.nova_informacao.setObjectName(u"nova_informacao")
        sizePolicy.setHeightForWidth(self.nova_informacao.sizePolicy().hasHeightForWidth())
        self.nova_informacao.setSizePolicy(sizePolicy)

        self.gridLayout_7.addWidget(self.nova_informacao, 0, 0, 1, 1)

        self.horizontalSpacer_5 = QSpacerItem(433, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_7.addItem(self.horizontalSpacer_5, 0, 1, 1, 1)

        self.exluidos_inf = QCheckBox(self.tab)
        self.exluidos_inf.setObjectName(u"exluidos_inf")

        self.gridLayout_7.addWidget(self.exluidos_inf, 0, 2, 1, 1)

        self.informacoes = QTableWidget(self.tab)
        if (self.informacoes.columnCount() < 1):
            self.informacoes.setColumnCount(1)
        __qtablewidgetitem16 = QTableWidgetItem()
        self.informacoes.setHorizontalHeaderItem(0, __qtablewidgetitem16)
        self.informacoes.setObjectName(u"informacoes")
        self.informacoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.informacoes.setAlternatingRowColors(True)
        self.informacoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.informacoes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.informacoes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.informacoes.horizontalHeader().setVisible(False)
        self.informacoes.horizontalHeader().setCascadingSectionResizes(True)
        self.informacoes.horizontalHeader().setMinimumSectionSize(115)
        self.informacoes.horizontalHeader().setDefaultSectionSize(115)
        self.informacoes.horizontalHeader().setHighlightSections(False)
        self.informacoes.horizontalHeader().setProperty("showSortIndicator", True)
        self.informacoes.horizontalHeader().setStretchLastSection(True)
        self.informacoes.verticalHeader().setVisible(False)

        self.gridLayout_7.addWidget(self.informacoes, 1, 0, 1, 3)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_3 = QGridLayout(self.tab_2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.locais = QTableWidget(self.tab_2)
        if (self.locais.columnCount() < 2):
            self.locais.setColumnCount(2)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.locais.setHorizontalHeaderItem(0, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.locais.setHorizontalHeaderItem(1, __qtablewidgetitem18)
        self.locais.setObjectName(u"locais")
        self.locais.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.locais.setAlternatingRowColors(True)
        self.locais.setSelectionMode(QAbstractItemView.SingleSelection)
        self.locais.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.locais.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.locais.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.locais.horizontalHeader().setHighlightSections(False)
        self.locais.verticalHeader().setVisible(False)
        self.locais.verticalHeader().setHighlightSections(False)

        self.gridLayout_3.addWidget(self.locais, 1, 0, 1, 1)

        self.frame = QFrame(self.tab_2)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_4)

        self.locais_excluidos = QCheckBox(self.frame)
        self.locais_excluidos.setObjectName(u"locais_excluidos")

        self.horizontalLayout_2.addWidget(self.locais_excluidos)


        self.gridLayout_3.addWidget(self.frame, 0, 0, 1, 1)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout.addWidget(self.tabWidget, 1, 2, 1, 2)

        QWidget.setTabOrder(self.tabWidget, self.avaliacoes)
        QWidget.setTabOrder(self.avaliacoes, self.novo_evento)
        QWidget.setTabOrder(self.novo_evento, self.excluidos)
        QWidget.setTabOrder(self.excluidos, self.eventos)
        QWidget.setTabOrder(self.eventos, self.nova_informacao)
        QWidget.setTabOrder(self.nova_informacao, self.exluidos_inf)
        QWidget.setTabOrder(self.exluidos_inf, self.informacoes)
        QWidget.setTabOrder(self.informacoes, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.locais)
        QWidget.setTabOrder(self.locais, self.locais_excluidos)
        QWidget.setTabOrder(self.locais_excluidos, self.alterar)
        QWidget.setTabOrder(self.alterar, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.pushButton)

        self.retranslateUi(Form)
        self.alterar.released.connect(Form.alterar)
        self.pushButton_4.released.connect(Form.historico)
        self.pushButton_3.released.connect(Form.close)
        self.avaliacoes.cellDoubleClicked.connect(Form.abrir_avaliacao)
        self.eventos.cellDoubleClicked.connect(Form.abrir_evento)
        self.novo_evento.clicked.connect(Form.novo_evento)
        self.excluidos.stateChanged.connect(Form.carregar_eventos)
        self.nova_informacao.released.connect(Form.nova_informacao)
        self.exluidos_inf.released.connect(Form.carregar_informacoes)
        self.informacoes.cellDoubleClicked.connect(Form.abrir_informacao)
        self.informacoes.cellEntered.connect(Form.abrir_informacao)
        self.pushButton.released.connect(Form.relatorio)
        self.pushButton_2.released.connect(Form.novo_local_de_trabalho)
        self.locais_excluidos.released.connect(Form.carregar_locais_de_trabalho)
        self.locais.cellDoubleClicked.connect(Form.abrir_local_de_trabalho)
        self.pushButton_5.released.connect(Form.resultado)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Servidor", None))
        self.alterar.setText(QCoreApplication.translate("Form", u"Alterar", None))
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"Fechar", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"Hist\u00f3rico", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Relat\u00f3rio", None))
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"Resultado", None))
        self.excluido.setText(QCoreApplication.translate("Form", u"<html><head/><body><p><span style=\" color:#ff0000;\">Excluido</span></p></body></html>", None))
        self.label.setText(QCoreApplication.translate("Form", u"Cargo:", None))
        self.cargo.setText(QCoreApplication.translate("Form", u"cargo", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Nome:", None))
        self.nome.setText(QCoreApplication.translate("Form", u"nome", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Form", u"matricula", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Data de admiss\u00e3o:", None))
        self.dataadmissao.setText(QCoreApplication.translate("Form", u"data de admiss\u00e3o", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        self.lotacao.setText(QCoreApplication.translate("Form", u"lotacao", None))
        ___qtablewidgetitem = self.avaliacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Data inicial", None));
        ___qtablewidgetitem1 = self.avaliacoes.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem2 = self.avaliacoes.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Formul\u00e1rio", None));
        ___qtablewidgetitem3 = self.avaliacoes.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Nome", None));
        ___qtablewidgetitem4 = self.avaliacoes.verticalHeaderItem(0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Form", u"New Row", None));
        ___qtablewidgetitem5 = self.avaliacoes.verticalHeaderItem(1)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Form", u"New Row", None));

        __sortingEnabled = self.avaliacoes.isSortingEnabled()
        self.avaliacoes.setSortingEnabled(False)
        ___qtablewidgetitem6 = self.avaliacoes.item(0, 0)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Form", u"ii", None));
        ___qtablewidgetitem7 = self.avaliacoes.item(0, 1)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("Form", u"ii", None));
        ___qtablewidgetitem8 = self.avaliacoes.item(0, 2)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("Form", u"ii", None));
        ___qtablewidgetitem9 = self.avaliacoes.item(0, 3)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("Form", u"ffffffffffffffffffffffffffffffffffffffffffffffffffff", None));
        self.avaliacoes.setSortingEnabled(__sortingEnabled)

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_avaliacoes), QCoreApplication.translate("Form", u"Avalia\u00e7\u00f5es", None))
        self.novo_evento.setText(QCoreApplication.translate("Form", u"Novo", None))
        ___qtablewidgetitem10 = self.eventos.horizontalHeaderItem(0)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("Form", u"Data inicial", None));
        ___qtablewidgetitem11 = self.eventos.horizontalHeaderItem(1)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem12 = self.eventos.horizontalHeaderItem(2)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("Form", u"Total de dias", None));
        ___qtablewidgetitem13 = self.eventos.horizontalHeaderItem(3)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("Form", u"Descri\u00e7\u00e3o", None));
        ___qtablewidgetitem14 = self.eventos.verticalHeaderItem(0)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("Form", u"1", None));
        ___qtablewidgetitem15 = self.eventos.verticalHeaderItem(1)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("Form", u"2", None));
        self.excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_eventos), QCoreApplication.translate("Form", u"Eventos", None))
        self.nova_informacao.setText(QCoreApplication.translate("Form", u"Nova", None))
        self.exluidos_inf.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        ___qtablewidgetitem16 = self.informacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("Form", u"T\u00edtulo", None));
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Form", u"Informa\u00e7\u00f5es", None))
        ___qtablewidgetitem17 = self.locais.horizontalHeaderItem(0)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("Form", u"Data de in\u00edcio", None));
        ___qtablewidgetitem18 = self.locais.horizontalHeaderItem(1)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None));
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"Novo", None))
        self.locais_excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Locais de trabalho", None))
    # retranslateUi

