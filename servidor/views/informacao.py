# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'informacao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(456, 286)
        Dialog.setMinimumSize(QSize(456, 286))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.titulo = QLineEdit(Dialog)
        self.titulo.setObjectName(u"titulo")
        self.titulo.setInputMethodHints(Qt.ImhUppercaseOnly|Qt.ImhUrlCharactersOnly)

        self.verticalLayout.addWidget(self.titulo)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.informacao = QPlainTextEdit(Dialog)
        self.informacao.setObjectName(u"informacao")
        self.informacao.setInputMethodHints(Qt.ImhUppercaseOnly)
        self.informacao.setTabChangesFocus(True)

        self.verticalLayout.addWidget(self.informacao)

        self.excluido = QLabel(Dialog)
        self.excluido.setObjectName(u"excluido")
        self.excluido.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.excluido)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.excluir = QPushButton(self.frame)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.historico = QPushButton(self.frame)
        self.historico.setObjectName(u"historico")

        self.horizontalLayout.addWidget(self.historico)

        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")

        self.horizontalLayout.addWidget(self.salvar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.frame)

        QWidget.setTabOrder(self.titulo, self.informacao)
        QWidget.setTabOrder(self.informacao, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.excluir)
        QWidget.setTabOrder(self.excluir, self.historico)
        QWidget.setTabOrder(self.historico, self.salvar)

        self.retranslateUi(Dialog)
        self.pushButton_2.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.accept)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00e3o", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"T\u00edtulo:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00e3o:", None))
        self.excluido.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0000;\">Excluido</span></p></body></html>", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

