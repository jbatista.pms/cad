# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'cadastro_local.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(400, 157)
        Dialog.setMinimumSize(QSize(400, 157))
        Dialog.setMaximumSize(QSize(400, 157))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.formLayout = QFormLayout(self.frame)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setLabelAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.formLayout.setContentsMargins(0, 0, 0, -1)
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.lotacao = QComboBox(self.frame)
        self.lotacao.setObjectName(u"lotacao")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.lotacao)

        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_3)

        self.sublotacao = QComboBox(self.frame)
        self.sublotacao.setObjectName(u"sublotacao")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.sublotacao)

        self.label_2 = QLabel(self.frame)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_2)

        self.data_inicio = QDateEdit(self.frame)
        self.data_inicio.setObjectName(u"data_inicio")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.data_inicio.sizePolicy().hasHeightForWidth())
        self.data_inicio.setSizePolicy(sizePolicy)
        self.data_inicio.setCalendarPopup(True)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.data_inicio)


        self.verticalLayout.addWidget(self.frame)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.informacao = QLabel(Dialog)
        self.informacao.setObjectName(u"informacao")
        self.informacao.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.informacao)

        self.frame_2 = QFrame(Dialog)
        self.frame_2.setObjectName(u"frame_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy1)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(self.frame_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.excluir = QPushButton(self.frame_2)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.historico = QPushButton(self.frame_2)
        self.historico.setObjectName(u"historico")

        self.horizontalLayout.addWidget(self.historico)

        self.salvar = QPushButton(self.frame_2)
        self.salvar.setObjectName(u"salvar")

        self.horizontalLayout.addWidget(self.salvar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.frame_2)

        QWidget.setTabOrder(self.lotacao, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.data_inicio)
        QWidget.setTabOrder(self.data_inicio, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.excluir)
        QWidget.setTabOrder(self.excluir, self.salvar)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro local de trabalho", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Sublota\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Data de in\u00edcio:", None))
        self.informacao.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0004;\">Informa\u00e7\u00e3o</span></p></body></html>", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

