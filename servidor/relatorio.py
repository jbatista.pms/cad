from collections import defaultdict, OrderedDict
from PySide2 import QtCore

from ComumComissoes.cargo.models import Cargo
from ComumComissoes.lotacao.controles_uteis import LotacaoSelecao
from ComumComissoes.lotacao.models import Lotacao
from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.qt import QTableWidgetItem
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows
from QtUtils.relatorio.dados import ModeloRelatorio

from avaliacao.models import Avaliacao
from evento.models import Evento
from situacao.models import Situacao
from . import servidor
from .cadastro import Cadastro
from .models import Informacao, Servidor
from .views import relatorio


class ServidorModeloRelatorio(ModeloRelatorio):
    dados = lambda ser: ser.__data__
    extras = ['cargo', 'lotacao',]
    modelo = Servidor
    funcoes = {
        'avaliacoes': lambda ser: Avaliacao.select2().filter(
            (Avaliacao.servidor==ser)&(Avaliacao.avaliacao==True)
        ),
        'eventos': lambda ser: Evento.select2().filter(Evento.servidor==ser),
        'informacoes': lambda ser: Informacao.select2().filter(Informacao.servidor==ser),
    }


class ServidorRelatorioJanela(subwindowsbase.Listar, LotacaoSelecao):
    classe_ui = relatorio.Ui_Form
    lotacao = None

    def __filtro_em_texto(self):
        if not self.__filtros:
            return 'Sem filtros.'
        texto = ''
        for k,v in self.__filtros.items():
            if texto:
                texto += '; '
            texto += "{k}: {v}".format(k=k,v=v)
        texto += '.'
        return texto

    def inicializar(self, *args, **kwargs):
        self.__filtros = {}
        self.servidores = {}
        # Preencher lista de cargos
        self.cargos = [i for i in Cargo.select2()]
        self.ui.cargo.addItem("Todos")
        self.ui.cargo.addItems([i.nome for i in self.cargos])
        # Preencher lista de situações
        self.situacoes = Situacao.select2()
        self.ui.situacao.addItem("Todos")
        self.ui.situacao.addItems([i.nome for i in self.situacoes])
        # Preencher lista de lotações
        self.carregar_lotacoes()

    def atualizar(self):
        fs = Servidor.select2()

        # Filtrar por situação
        situacao = self.ui.situacao.currentIndex()
        if situacao>0:
            situacao = self.situacoes[situacao-1]
            fs = fs.where(Servidor.situacao==situacao)
            self.__filtros.update({'Situação': str(situacao)})
        else:
            self.__filtros.update({'Situação': 'Todas'})

        # Filtrar por cargo
        cargo = self.ui.cargo.currentIndex()
        if cargo>0:
            cargo = self.cargos[cargo-1]
            fs = fs.where(Servidor.cargo==cargo)
            self.__filtros.update({'Cargo': str(cargo)})
        else:
            self.__filtros.update({'Cargo': 'Todos'})

        # Filtrar por lotação 
        lotacao = self.lotacao_indice()
        sublotacao = self.sublotacao_indice()
        if lotacao > 0:
            lotacao = self.lotacao_selecionada()
            self.__filtros.update({'Lotação': str(lotacao)})
            if sublotacao<1:
                fs = fs.join(Lotacao)
                fs = fs.where(
                    (Lotacao.pai==lotacao) |
                    (Servidor.lotacao==lotacao)
                )
                self.__filtros.update({'Sublotação': 'Todas'})
            elif sublotacao>1:
                sublotacao = self.sublotacao_selecionada()
                fs = fs.where(Servidor.lotacao==sublotacao)
                self.__filtros.update({'Sublotação': str(sublotacao)})
            elif sublotacao==1:
                fs = fs.where(Servidor.lotacao==lotacao)
                self.__filtros.update({'Sublotação': 'Somente'})
        else:
            self.__filtros.update({'Lotação': 'Todas'})
            if 'Sublotação' in self.__filtros:
                self.__filtros.pop('Sublotação')

        # Quantidade 
        quantidade = fs.count()
        if quantidade == 1:
            self.ui.contador.setText("%i registro econtrado" % quantidade)
        else:
            self.ui.contador.setText("%i registros econtrados" % quantidade)

        # Popular tabela
        self.ui.tabela.setRowCount(quantidade)
        self.servidores = {str(s.id):s for s in fs.objects()}
        for i,ser in enumerate(self.servidores.values()):
            # Id
            item = QTableWidgetItem()
            item.setText(str(ser.id))
            self.ui.tabela.setVerticalHeaderItem(i, item)
            # Nome
            item = QTableWidgetItem()
            item.setText(ser.nome)
            self.ui.tabela.setItem(i, 0, item)
            # Matrícula
            item = QTableWidgetItem()
            item.setText(ser.matricula)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.tabela.setItem(i, 1, item)
            # Cargo
            item = QTableWidgetItem()
            item.setText(ser.cargo.nome)
            self.ui.tabela.setItem(i, 2, item)
            # Lotação
            item = QTableWidgetItem()
            item.setText(str(ser.local))
            self.ui.tabela.setItem(i, 3, item)
        self.ui.tabela.resizeColumnsToContents()

    def abrir(self, row, column):
        id_ = self.ui.tabela.verticalHeaderItem(row).text()
        instance = self.servidores[id_]
        servidor.Control(self, instance=instance).show()
    
    def alerta_sem_dados(self):
        msg = Alerta(parent=self)
        msg.setText("Nenhuma relação de servidores\npara impressão.")
        msg.exec_()

    def cadastrar(self):
        Cadastro(self).exec_()
    
    def dados_lotacoes(self):
        # Apurando
        lista = defaultdict(list)
        for ser in self.servidores.values():
            lista[ser.lotacao_orgao.nome].append(self.dados_servidor(ser))
        # Ordenando
        lotacoes = []
        for lot in sorted(lista):
            dados_servidores = lista[lot]
            sorted(dados_servidores, key=lambda s:s['nome'])
            lotacoes.append({
                'nome': lot,
                'servidores': dados_servidores,
            })
        return lotacoes
    
    def dados_sublotacoes(self):
        # Apurando
        lista = defaultdict(list)
        for ser in self.servidores.values():
            lista[str(ser.local)].append(self.dados_servidor(ser))
        # Ordenando
        lotacoes = []
        for lot in sorted(lista):
            dados_servidores = lista[lot]
            sorted(dados_servidores, key=lambda s:s['nome'])
            lotacoes.append({
                'nome': lot,
                'servidores': dados_servidores,
            })
        return lotacoes
    
    def dados_servidor(self, ser: Servidor):
        return {
            'cargo': str(ser.cargo),
            'lotacao': str(ser.local),
            'matricula': ser.matricula,
            'nome': ser.nome,
            'situacao': str(ser.situacao),
        }
    
    def dados_servidores(self):
        return [self.dados_servidor(ser=s) for s in self.servidores.values()]
    
    def impressao_por_local(self, lotacoes_func: list, nome: str):
        if self.servidores:
            objetos = [{
                'filtros': self.__filtro_em_texto(),
                'lotacoes': lotacoes_func(),
                'nome_relatorio': nome,
                'total': len(self.servidores),
            }]
            relatorio = Relatorio(
                parent=self,
                objetos=objetos,
                template='servidor/templates/relatorio_local.odt',
            )
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()
        else:
            self.alerta_sem_dados()
    
    def impressao_por_lotacao(self):
        self.impressao_por_local(
            lotacoes_func=self.dados_lotacoes,
            nome='POR LOTAÇÃO'
        )
    
    def impressao_por_sublotacao(self):
        self.impressao_por_local(
            lotacoes_func=self.dados_sublotacoes,
            nome='POR LOCAL DE TRABALHO'
        )
    
    def impressao_todos(self):
        if self.servidores:
            objetos = [{
                'servidores': self.dados_servidores(),
                'total': len(self.servidores),
                'filtros': self.__filtro_em_texto(),
            }]
            relatorio = Relatorio(
                parent=self,
                objetos=objetos,
                template='servidor/templates/relatorio_todos.odt',
            )
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()
        else:
            self.alerta_sem_dados()

    def limpar_cargo(self):
        self.ui.cargo.setCurrentIndex(0)

    def limpar_lotacao(self):
        self.ui.lotacao.setCurrentIndex(0)

    def limpar_sublotacao(self):
        self.ui.sublotacao.setCurrentIndex(0)
    
    def limpar_situacao(self):
        self.ui.situacao.setCurrentIndex(0)

    def receber_sinal_atualizacao(self, instance):
        if type(instance) == Servidor:
            self.atualizar()

    def receber_sinal_exclusao(self, instance):
        if type(instance) == Servidor:
            self.atualizar()
