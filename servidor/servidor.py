from datetime import datetime
from PySide2 import QtCore

from QtUtils.db import DataBase
from QtUtils.historico import historico
from QtUtils import subwindowsbase, qt
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows

from avaliacao import avaliacao
from avaliacao.colecoes import AVALIACOES
from avaliacao.models import Avaliacao
from avaliacao.utils import estimar_datas_servidor
from evento.models import Evento
from evento import evento
from .cadastro import Cadastro
from .cadastro_local import CadastroLocalDeTrabalho
from .informacao import InformacaoFormulario
from .models import Informacao, LocalDeTrabalho
from .resultado import RelatorioFormularioResultadoFinal
from .views.servidor import *


class Control(subwindowsbase.Listar):
    classe_ui = Ui_Form
    encontrado = True

    def abrir_avaliacao(self, row, column):
        id_av = self.ui.avaliacoes.verticalHeaderItem(row).text()
        instance = self.dict_avaliacao[id_av]
        avaliacao.Control(self, instance=instance).exec_()

    def abrir_evento(self, row, column):
        evento_id = self.ui.eventos.verticalHeaderItem(row).text()
        instance = self.dict_eventos[evento_id]
        with DataBase().obter_database().atomic() as db_atomic:
            if evento.Control(self, instance=instance).exec_():
                self.carregar_eventos()
                self.atualizar_avaliacoes()

    def abrir_informacao(self, row, column):
        informacao_id = self.ui.informacoes.verticalHeaderItem(row).text()
        instance = self.dict_informacoes[informacao_id]
        with DataBase().obter_database().atomic() as db_atomic:
            if InformacaoFormulario(self, instance=instance).exec_():
                self.carregar_informacoes()
                self.atualizar_avaliacoes()
    
    def abrir_local_de_trabalho(self, row, column):
        id_local = self.ui.locais.verticalHeaderItem(row).text()
        instance = self.dict_locais[id_local]
        CadastroLocalDeTrabalho(self, instance=instance).exec_()

    def alterar(self):
        Cadastro(self, instance=self.instance).exec_()

    def atualizar_avaliacoes(self):
        estimar_datas_servidor(self.instance)
        self.carregar_avaliacoes()

    def carregar_avaliacao(self, indice, avaliacao):
        descricao_avaliacao = AVALIACOES[avaliacao.ordem]['descricao']
        self.ui.avaliacoes.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.avaliacoes.verticalHeaderItem(indice).setText(
            str(avaliacao.id)
        )
        if avaliacao.indeterminado:
            if avaliacao.anterior and avaliacao.anterior.indeterminado:
                data_inicial = "Indeterminado"
            else:
                data_inicial = avaliacao.strftime('data_inicial')
            data_final ="Indeterminado"
        else:
            data_inicial = avaliacao.strftime('data_inicial')
            data_final = avaliacao.strftime('data_final')
        self.ui.avaliacoes.item(indice,0).setText(data_inicial)
        self.ui.avaliacoes.item(indice,1).setText(data_final)
        if avaliacao.ficha_devolvida:
            self.ui.avaliacoes.item(indice,2).setText("<- Devolvido")
        elif avaliacao.ficha_enviada:
            self.ui.avaliacoes.item(indice,2).setText("-> Enviado")
        self.ui.avaliacoes.item(indice,3).setText(descricao_avaliacao)

        for i in range(0,2):
            celula_data(self.ui.avaliacoes.item(indice,i))

    def carregar_avaliacoes(self):
        avaliacoes = Avaliacao.select2(excluidos=self.instance.data_exclusao!=None).where(
            (Avaliacao.servidor==self.instance) &
            (Avaliacao.data_inicial<=datetime.today()) &
            (Avaliacao.indeterminado==False)
        ).objects()
        self.ui.avaliacoes.setRowCount(avaliacoes.count())
        for f in range(avaliacoes.count()):
            self.ui.avaliacoes.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            self.ui.avaliacoes.setItem(f,0, qt.QTableWidgetItem())
            self.ui.avaliacoes.setItem(f,1, qt.QTableWidgetItem())
            self.ui.avaliacoes.setItem(f,2, qt.QTableWidgetItem())
            self.ui.avaliacoes.setItem(f,3, qt.QTableWidgetItem())
            self.carregar_avaliacao(f, avaliacoes[f])
        self.ui.avaliacoes.resizeColumnsToContents()
        
        self.dict_avaliacao = {str(a.id):a for a in avaliacoes}
    
    def carregar_locais_de_trabalho(self):
        locais = LocalDeTrabalho.select2(excluidos=self.ui.locais_excluidos.isChecked()).where(
            LocalDeTrabalho.servidor==self.instance
        ).objects()
        self.ui.locais.setRowCount(len(locais))
        for f in range(len(locais)):
            self.ui.locais.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            for i in range(0,2):
                self.ui.locais.setItem(f,i, qt.QTableWidgetItem())
            self.carregar_local_de_trabalho(f, locais[f])
        self.ui.locais.resizeColumnsToContents()
        self.dict_locais = {str(e.id):e for e in locais}

    def carregar_local_de_trabalho(self, indice, local):
        self.ui.locais.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.locais.verticalHeaderItem(indice).setText(
            str(local.id)
        )
        self.ui.locais.item(indice,0).setText(local.strftime('data_inicio'))
        self.ui.locais.item(indice,1).setText(str(local))

        celula_data(self.ui.locais.item(indice,0))

    def carregar_dados(self):
        self.setWindowTitle(self.instance.nome)
        self.ui.nome.setText(self.instance.nome)
        self.ui.matricula.setText(self.instance.matricula)
        self.ui.dataadmissao.setText(self.instance.strftime('data_admissao'))
        self.ui.cargo.setText(self.instance.cargo.nome)
        self.ui.lotacao.setText(self.instance.lotacao.descricao)
        self.ui.excluido.hide()
        if self.instance.data_exclusao:
            self.ui.alterar.setEnabled(False)
            self.ui.novo_evento.setEnabled(False)
            self.ui.nova_informacao.setEnabled(False)
            self.ui.excluido.show()

    def carregar_evento(self, indice, evento):
        self.ui.eventos.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.eventos.verticalHeaderItem(indice).setText(
            str(evento.id)
        )
        self.ui.eventos.item(indice,0).setText(evento.strftime('data_inicial'))
        if evento.indeterminado:
            self.ui.eventos.item(indice,1).setText("Indeterminado")
            self.ui.eventos.item(indice,2).setText("∞")
        else:
            self.ui.eventos.item(indice,1).setText(
                evento.strftime('data_final')
                )
            self.ui.eventos.item(indice,2).setText(str(evento.total))
        self.ui.eventos.item(indice,3).setText(evento.descricao)

        for i in range(0,3):
            celula_data(self.ui.eventos.item(indice,i))

    def carregar_eventos(self):
        eventos = Evento.select2(excluidos=self.ui.excluidos.isChecked()).where(
            Evento.servidor==self.instance
        ).objects()
        self.ui.eventos.setRowCount(len(eventos))
        for f in range(len(eventos)):
            self.ui.eventos.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            for i in range(0,4):
                self.ui.eventos.setItem(f,i, qt.QTableWidgetItem())
            self.carregar_evento(f, eventos[f])
        self.ui.eventos.resizeColumnsToContents()
        self.dict_eventos = {str(e.id):e for e in eventos}
    
    def carregar_informacao(self, indice, informacao):
        self.ui.informacoes.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.informacoes.verticalHeaderItem(indice).setText(
            str(informacao.id)
        )
        self.ui.informacoes.item(indice,0).setText(informacao.titulo)
    
    def carregar_informacoes(self):
        informacoes = Informacao.select2(excluidos=self.ui.exluidos_inf.isChecked()).where(
            Informacao.servidor==self.instance
        ).objects()
        self.ui.informacoes.setRowCount(len(informacoes))
        for f in range(len(informacoes)):
            self.ui.informacoes.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            self.ui.informacoes.setItem(f,0, qt.QTableWidgetItem())
            self.carregar_informacao(f, informacoes[f])
        self.dict_informacoes = {str(e.id):e for e in informacoes}

    def inicializar(self, *args, **kwargs):
        self.carregar_dados()
        self.carregar_avaliacoes()
        self.carregar_eventos()
        self.carregar_informacoes()
        self.carregar_locais_de_trabalho()
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()
    
    def nova_informacao(self):
        with DataBase().obter_database().atomic() as db_atomic:
            if InformacaoFormulario(self, instance=Informacao(servidor=self.instance)).exec_():
                self.carregar_informacoes()
    
    def novo_local_de_trabalho(self):
        if CadastroLocalDeTrabalho(self, instance=LocalDeTrabalho(servidor=self.instance)).exec_():
            self.carregar_locais_de_trabalho()

    def novo_evento(self):
        with DataBase().obter_database().atomic() as db_atomic:
            if evento.Control(self, instance=Evento(servidor=self.instance)).exec_():
                self.carregar_eventos()
                self.atualizar_avaliacoes()

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, Avaliacao) and instance.servidor == self.instance:
            self.carregar_avaliacoes()
        elif isinstance(instance, LocalDeTrabalho) and instance.servidor == self.instance:
            self.carregar_dados()
            self.carregar_locais_de_trabalho()
            self.carregar_avaliacoes()
        elif instance == self.instance:
            self.instance = instance
            self.inicializar()
    
    def relatorio(self):
        # Informações
        informacoes = []
        for inf in Informacao.select2().filter(Informacao.servidor==self.instance):
            informacoes.append({'informacao': inf.informacao})
        # Eventos
        eventos = []
        for ev in Evento.select2().filter(Evento.servidor==self.instance):
            eventos.append({
                'data_inicial': ev.data_inicial,
                'data_final': ev.data_final,
                'descricao': ev.descricao,
            })
        # Avaliações
        avaliacoes = []
        avaliacoes_filtro = (Avaliacao.avaliacao==True) & (Avaliacao.servidor==self.instance)
        for av in Avaliacao.select2().where(avaliacoes_filtro):
            avaliacoes.append({
                'data_inicial': av.data_inicial or '--------------',
                'data_final': av.data_final or '--------------',
                'data_enviada': av.data_enviada or '--------------',
                'data_devolvida': av.data_devolvida or '--------------',
                'assiduidade': av.assiduidade or '--------------',
                'disciplina': av.disciplina or '--------------',
                'iniciativa': av.iniciativa or '--------------',
                'produtividade': av.produtividade or '--------------',
                'responsabilidade': av.responsabilidade or '--------------',
                'nota': av.nota or '--------------',
                'conceito': av.obter_conceito,
                'lotacao': str(av.local),
                'anotacoes': av.observacao.split('\n') if av.observacao else ['Sem anotações.',],
                'descricao': AVALIACOES[av.ordem]['descricao'],
            })
        objetos = [{
            'servidor': {
                'nome': self.instance.nome,
                'cargo': str(self.instance.cargo),
                'matricula': self.instance.matricula,
                'lotacao': self.instance.lotacao.descricao,
            },
            'avaliacoes': avaliacoes,
            'eventos': eventos,
            'informacoes': informacoes,
        }]
        relatorio = Relatorio(
            parent=self,
            objetos=objetos,
            template='servidor/templates/servidor.odt',
        )
        relatorio.exec_()
        if relatorio.documento:
            RelatorioViewSubWindows(self, relatorio.documento).show()
    
    def resultado(self):
        return RelatorioFormularioResultadoFinal(parent=self, objetos=[self.instance]).mostrar_sub_janela()
        
    def teste_unica(self, windows):
        if windows.instance.matricula == self.instance.matricula:
            return True
        return False


def celula_data(celula):
    celula.setTextAlignment(
        QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter
    )
