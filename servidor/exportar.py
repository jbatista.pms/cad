import csv
from PySide2 import QtWidgets

from QtUtils.colecoes.dialogos import Alerta

from .models import Servidor

def exportar_servidores(parent, servidores=Servidor.select2()):
    # Selecionar arquivo destino
    arq_nome = QtWidgets.QFileDialog.getSaveFileName(parent,
            "Exportar avaliações", '',
            "CSV com cabeçalho (*.csv);;All Files (*)")
    if not arq_nome[0]:
        return

    # Preparar arquivo
    try:    
        arq = open(arq_nome[0], 'w')
    except PermissionError as a:
        return Alerta(
            parent,
            text=(
                "Não foi possível salvar!\n\n"
                "O arquivo está aberto em outro programa ou sendo utilizado por algum processo.\n"
            )
        ).exec_()

    # Popular linhas
    linhas = []
    for ser in servidores:
        linhas.append(ser.dados())

    if linhas:
        arq_csv = csv.DictWriter(
            arq, 
            fieldnames=list(linhas[0].keys()),
            delimiter=';', 
            lineterminator='\n'
        )
        arq_csv.writeheader()
        arq_csv.writerows(linhas)
        arq.close()