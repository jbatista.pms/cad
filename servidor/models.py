from collections import OrderedDict
from typing import Dict, Optional

from peewee import *

from ComumComissoes.cargo.models import Cargo
from ComumComissoes.lotacao.models import Lotacao
from QtUtils.db import DataBase
from QtUtils.historico.models import Historico

from situacao.models import Situacao

def criar_local(servidor, lotacao, data_inicio=None):
    return LocalDeTrabalho(
        servidor=servidor,
        lotacao=lotacao,
        data_inicio=data_inicio or DataBase().data(),
    )

def servidor_situacao_padrao():
    situacao, _ = Situacao.get_or_create(
        nome='ATIVO',
        data_exclusao=None,
    )
    return situacao


class Servidor(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    nome = CharField()
    matricula = CharField()
    estagio_interrompido = BooleanField(default=False)
    data_admissao = DateField()
    cargo = ForeignKeyField(Cargo, related_name='servidores')
    lotacao = ForeignKeyField(Lotacao)
    situacao = ForeignKeyField(Situacao, default=servidor_situacao_padrao, null=True, related_name='servidores')
    # TODO: Campo situação não deverá aceitar valores nulos futuramente (28/07/2021).

    def __str__(self):
        return str(self.nome)
    
    @staticmethod
    def campos_ignorar():
        return ['desativado','origem','user','data_exclusao','situacao','estagio_interrompido']
    
    def dados(self):
        dados = OrderedDict()
        dados['nome'] = self.nome
        dados['matricula'] = self.matricula
        dados['data_admissao'] = self.data_admissao
        dados['estagio_interrompido'] = self.estagio_interrompido
        dados['situacao'] = self.situacao.nome
        dados['cargo'] = self.cargo.nome
        dados['lotacao'] = self.lotacao_orgao
        dados['sublotacao'] = self.lotacao if self.lotacao.pai else ''
        return dados

    @property
    def local(self):
        return LocalDeTrabalho.select2().filter(
            LocalDeTrabalho.servidor==self,
        ).first()
    
    def lotacao_na_data(self, data):
        return LocalDeTrabalho.select2().filter(
            (LocalDeTrabalho.servidor==self) & (LocalDeTrabalho.data_inicio<=data)
        ).first()
    
    @property
    def lotacao_orgao(self):
        return self.lotacao.pai or self.lotacao
    
    @staticmethod
    def exportar_extras():
        return ['lotacao', 'cargo', 'situacao']
    
    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(cls.nome)
    
    def validar(self) -> Optional[Dict]:
        erros = {}
        if self.id:
            if self.situacao is None:
                erros.update({'Situação': 'Não preenchido.'})
        if self.nome is None:
            erros.update({'Nome': 'Não preenchido.'})
        if self.matricula is None:
            erros.update({'Matrícula': 'Não preenchido.'})
        if self.data_admissao is None:
            erros.update({'Data de admissão': 'Não preenchido.'})
        if self.cargo is None:
            erros.update({'Cargo': 'Não preenchido.'})
        if self.lotacao is None:
            erros.update({'Lotação': 'Não preenchido.'})
        return erros


class LocalDeTrabalho(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    servidor = ForeignKeyField(Servidor, related_name='lotacoes')
    lotacao = ForeignKeyField(Lotacao, related_name='lotacoes')
    data_inicio = DateField()

    # TODO: Restringir criação a período superior a última avaliação enviada

    def __str__(self):
        if self.lotacao.pai:
            return f"{self.lotacao.pai.nome} ({self.lotacao.nome})"
        return self.lotacao.nome
    
    @staticmethod
    def exportar_extras():
        return ['lotacao', 'servidor',]

    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(-cls.data_inicio)


class Informacao(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    servidor = ForeignKeyField(Servidor, related_name='informacoes')
    titulo = CharField()
    informacao = TextField()

    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(-cls.data_criacao)


from . import signals