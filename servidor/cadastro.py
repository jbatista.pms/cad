from ComumComissoes.cargo.models import Cargo
from ComumComissoes.lotacao.controles_uteis import LotacaoSelecao
from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Alerta, Confirmacao
from QtUtils.date import datetime_qdate, qdate_datetime
from QtUtils.db import DataBase
from QtUtils.validadores import TextoEmMaisculo

from situacao.models import Situacao
from .models import criar_local, LocalDeTrabalho, Servidor
from .views.alterar import Ui_Dialog


class Cadastro(subwindowsbase.Formulario, LotacaoSelecao):
    classe_ui = Ui_Dialog
    selecao_busca = False

    def accept(self):
        self.instance.nome = self.ui.nome.text()
        self.instance.matricula = self.ui.matricula.text()
        self.instance.data_admissao = qdate_datetime(self.ui.data_admissao.date())
        self.instance.cargo = self.cargos[self.ui.cargo.currentIndex()]
        self.instance.lotacao = self.locatao_resultado()
        if self.instance.id:
            self.instance.situacao = self.situacoes[self.ui.situacao.currentIndex()]
        
        # Teste de preenchimento de local de lotação
        if not self.locatao_resultado():
            return Alerta(self,text="Uma lotação precisa ser selecionada!").exec_()
        
        # Teste para saber se todos os campos foram preenchidos
        erros = self.instance.validar()
        if erros:
            msg = ''.join([f'{c},' for c in erros.keys()[:-1]])
            return Alerta(self,text=f"Preencha o campos {msg}!").exec_()

        with DataBase().obter_database().atomic():
            self.instance.save()
            self.enviar_sinal_atualizacao(self.instance)
        super().accept()

    def carregar_cargo(self):
        self.cargos = list(Cargo.select2().order_by(Cargo.nome))
        self.ui.cargo.addItems([i.nome for i in self.cargos])
        if self.instance.id:
            self.ui.cargo.setCurrentIndex(self.cargos.index(self.instance.cargo))

    def carregar_situacao(self):
        if self.instance.id:
            self.situacoes = list(Situacao.select2().order_by(Situacao.nome))
            self.ui.situacao.addItems([i.nome for i in self.situacoes])
            self.ui.situacao.setCurrentIndex(self.situacoes.index(self.instance.situacao))

    def excluir(self):
        if Confirmacao(self, text="Excluir servidor %s?" % self.instance.nome).exec_():
            self.instance.excluir_instancia()
            self.enviar_sinal_atualizacao(self.instance)
            self.close()

    def inicializar(self, *args, **kwargs):
        self.ui.nome.setValidator(TextoEmMaisculo(self))
        self.ui.matricula.setValidator(TextoEmMaisculo(self))
        self.carregar_lotacoes()
        if self.instance.id:
            self.setWindowTitle("Alterar cadastro de servidor")
            self.ui.nome.setText(self.instance.nome)
            self.ui.matricula.setText(self.instance.matricula)
            self.ui.data_admissao.setDate(datetime_qdate(self.instance.data_admissao))
            self.selecionar_lotacao(self.instance.lotacao)
        else:
            self.ui.excluir.hide()
            self.ui.situacao.setEnabled(False)
        self.carregar_cargo()
        self.carregar_situacao()

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', Servidor())