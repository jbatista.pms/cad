from pprint import pprint
from loguru import logger
from playhouse.shortcuts import model_to_dict
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows

from avaliacao.models import Avaliacao


class RelatorioFormularioResultadoFinal(Relatorio):
    def __init__(self, parent, objetos):
        super().__init__(
            parent=parent,
            objetos=self._objetos(objetos),
            template='servidor/templates/resultado_final.odt',
        )
    
    def _objetos(self, objetos):
        result = []
        for instancia in objetos:
            objeto = model_to_dict(instancia)
            # Avaliações
            avaliacoes_filtro = (Avaliacao.avaliacao==True) & (Avaliacao.servidor==instancia)
            for a in Avaliacao.select2().where(avaliacoes_filtro):
                key = 'av' + str(a.ordem//2)
                logger.debug(f'Coletando dados de {key}.')
                objeto.update({key: model_to_dict(a)})
                if a.ficha_devolvida:
                    objeto.update({
                        key: {
                            'assiduidade': objeto[key]['assiduidade'] or 0,
                            'disciplina': objeto[key]['disciplina'] or 0,
                            'iniciativa': objeto[key]['iniciativa'] or 0,
                            'produtividade': objeto[key]['produtividade'] or 0,
                            'responsabilidade': objeto[key]['responsabilidade'] or 0,
                            'nota': a.nota,
                            'conceito': a.obter_conceito,
                        }
                    })
                else:
                    objeto.update({
                        key: {
                            'assiduidade': '-*-',
                            'disciplina': '-*-',
                            'iniciativa': '-*-',
                            'produtividade': '-*-',
                            'responsabilidade': '-*-',
                            'nota': '-*-',
                            'conceito': '-*-',
                        }
                    })
            result.append(objeto)
        logger.debug(f'Finalizado coleta dos dados de {len(result)} objeto(s).')
        return result
    
    def mostrar_sub_janela(self):
        self.exec_()
        RelatorioViewSubWindows(self.parent(), self.documento).show()