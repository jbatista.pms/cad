from peewee import *

from QtUtils.historico.models import Historico


class Situacao(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    nome = CharField()

    def __str__(self):
        return self.nome
    
    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(cls.nome)