from QtUtils.subwindowsbase import Listar

from .cadastro import CadastroSituacao
from .models import Situacao
from .templates import listar


class ListarSituacao(Listar):
    classe_ui = listar.Ui_Form
    
    def cadastro(self, item):
        CadastroSituacao(self, instance=self.situacoes[item.row()]).exec_()

    def inicializar(self, *args, **kwargs):
        self.situacoes = Situacao.select2(excluidos=self.ui.excluidos.isChecked())
        self.ui.lista.clear()
        self.ui.lista.addItems([s.nome for s in self.situacoes])

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, Situacao):
            self.inicializar()