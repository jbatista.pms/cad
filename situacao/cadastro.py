from QtUtils.colecoes.dialogos import Alerta, Confirmacao
from QtUtils.historico import historico
from QtUtils.subwindowsbase import Formulario
from QtUtils.validadores import TextoEmMaisculo

from servidor.models import Servidor
from .models import Situacao
from .templates import cadastro


class CadastroSituacao(Formulario):
    classe_ui = cadastro.Ui_Dialog

    def excluir(self):
        if Servidor.select2().filter(Servidor.situacao==self.instance).count():
            return Alerta(
                parent=self,
                text=(
                    "Não é possível a exclusão!\n\n"
                    "Existem servidores classificados nesta situação."
                )

            ).exec_()
        mensagem = (
            "Deseja prosseguir com a exclusão:\n\n"
            "A ação e irreversível!"
        )
        if Confirmacao(self, title="Excluir situação?", text=mensagem).exec_():
            self.instance.excluir_instancia()
            self.enviar_sinal_atualizacao(self.instance)
            self.accept()

    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()
    
    def inicializar(self, *args, **kwargs):
        self.ui.nome.setValidator(TextoEmMaisculo(self))
        if self.instance.id:
            self.ui.nome.setText(self.instance.nome)
        else:
            self.ui.excluir.hide()
            self.ui.historico.hide()
    
    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', Situacao())

    def salvar(self):
        nome = self.ui.nome.text()
        if nome:
            self.instance.nome = nome
            self.instance.save()
            self.enviar_sinal_atualizacao(self.instance)
            self.accept()
        else:
            Alerta(
                parent=self,
                text="Campo nome não preenchido.",
            ).exec_()