# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'cadastro.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(344, 71)
        Dialog.setMaximumSize(QSize(344, 71))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.formLayout = QFormLayout(self.frame)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.nome = QLineEdit(self.frame)
        self.nome.setObjectName(u"nome")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.nome)


        self.verticalLayout.addWidget(self.frame)

        self.frame_2 = QFrame(Dialog)
        self.frame_2.setObjectName(u"frame_2")
        sizePolicy.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_3 = QPushButton(self.frame_2)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout.addWidget(self.pushButton_3)

        self.excluir = QPushButton(self.frame_2)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.historico = QPushButton(self.frame_2)
        self.historico.setObjectName(u"historico")

        self.horizontalLayout.addWidget(self.historico)

        self.pushButton = QPushButton(self.frame_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.frame_2)

        QWidget.setTabOrder(self.nome, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.excluir)
        QWidget.setTabOrder(self.excluir, self.historico)
        QWidget.setTabOrder(self.historico, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton_3.released.connect(Dialog.reject)
        self.pushButton.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de Situa\u00e7\u00e3o", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

