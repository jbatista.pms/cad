# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(234, 344)
        Form.setMinimumSize(QSize(234, 344))
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.excluidos = QCheckBox(Form)
        self.excluidos.setObjectName(u"excluidos")

        self.verticalLayout.addWidget(self.excluidos)

        self.lista = QListWidget(Form)
        self.lista.setObjectName(u"lista")

        self.verticalLayout.addWidget(self.lista)


        self.retranslateUi(Form)
        self.lista.doubleClicked.connect(Form.cadastro)
        self.excluidos.released.connect(Form.inicializar)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Tipos de situa\u00e7\u00e3o de servidores", None))
        self.excluidos.setText(QCoreApplication.translate("Form", u"Excluidos", None))
    # retranslateUi

