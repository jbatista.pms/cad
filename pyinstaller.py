"""
This is an example of using PyInstaller packager to build
executable from one of CEF Python's examples (wxpython.py).

See README-pyinstaller.md for installing required packages.

To package example type: `python pyinstaller.py`.
"""
import git
import os
import platform
import shutil
import sys
from subprocess import Popen

from loguru import logger

try:
    import PyInstaller
except ImportError:
    PyInstaller = None
    raise SystemExit("Error: PyInstaller package missing. "
                     "To install type: pip install --upgrade pyinstaller")

from QtUtils import QtUtils
from QtUtils.atualizacao.controles import DadosInstalacao
from QtUtils.modulos import ImportarClasse
from executar import definicoes_cliente

definicoes_cliente.update({'verificar_atualizacao': False,})
QtUtils.configurar(definicoes_cliente)

BUILD_DIR = os.path.join(os.getcwd(), "dev", "build")
EXE_EXT = ".exe"
DIST_DIR = os.path.join(os.getcwd(), "dev", "dist")

version = git.Repo(os.getcwd()).tags[-1].tag.tag
logger.debug(f"Versão para o instalador: {version}")

def main():
    # Platforms supported
    if platform.system() != "Windows":
        raise SystemExit("Error: Only Windows platform is currently "
                         "supported. See Issue #135 for details.")

    # Make sure nothing is cached from previous build.
    # Delete the build/ and dist/ directories.
    if os.path.exists(BUILD_DIR):
        shutil.rmtree(BUILD_DIR)
    if os.path.exists(DIST_DIR):
        shutil.rmtree(DIST_DIR)

    # Execute pyinstaller.
    # Note: the "--clean" flag passed to PyInstaller will delete
    #       global global cache and temporary files from previous
    #       runs. For example on Windows this will delete the
    #       "%appdata%/roaming/pyinstaller/bincache00_py27_32bit"
    #       directory.
    env = os.environ    
    if "--debug" in sys.argv:
        env["CEFPYTHON_PYINSTALLER_DEBUG"] = "1"
    # Acrescentar módulos para importação
    env["QTUTILS_MODULOS"] = ','.join(ImportarClasse.modulos_registrados())
    sub = Popen(
        [
            "pyinstaller", 
            "--clean", 
            f"--distpath={DIST_DIR}", 
            f"--workpath={BUILD_DIR}", 
            os.path.join(os.getcwd(), 'construir', 'pyinstaller.spec'),
            "--specpath=./dev"
        ], 
        env=env
    )
    sub.communicate()
    rcode = sub.returncode
    if rcode != 0:
        print("Error: PyInstaller failed, code=%s" % rcode)
        # Delete distribution directory if created
        if os.path.exists(DIST_DIR):
            shutil.rmtree(DIST_DIR)
        sys.exit(1)

    # Make sure everything went fine
    curdir = os.getcwd()
    app_dir = os.path.join(curdir, "dev", "dist", QtUtils.definicoes.abreviatura_nome)
    executable = os.path.join(app_dir, QtUtils.definicoes.abreviatura_nome + EXE_EXT)
    if not os.path.exists(executable):
        print("Error: PyInstaller failed, main executable is missing: %s"
              % executable)
        sys.exit(1)
    
    # Criar aquivo com dados da instalação
    arq_dados = os.path.join(
        DIST_DIR,
        f"{QtUtils.definicoes.abreviatura_nome}",
        "instalacao.ver",
    )
    DadosInstalacao(arquivo_instalacao=arq_dados).gravar_arquivo(versao=version)
    
    # On Windows open folder in explorer or when --debug is passed
    # run the result binary using "cmd.exe /k cefapp.exe", so that
    # console window doesn't close.
    if platform.system() == "Windows":
        if "--debug" in sys.argv:
            os.system("start cmd /k \"%s\"" % executable)
        else:
            # Done
            print("OK. Created dist/ directory.")
            build = Popen(
                [
                    "iscc",
                    "/DMyAppVersion=%s" % version,
                    "/DMyAppName=%s" % QtUtils.definicoes.abreviatura_nome,
                    "/DMyWork=%s" % os.getcwd(),
                    os.path.join(os.getcwd(), 'construir', 'instalador.iss'),
                ],
                env=env,
            )
            build.communicate()
            rcode = build.returncode
            if rcode != 0:
                print("Error: Inno setup compile failed, code=%s" % rcode)
                # Delete distribution directory if created
                if os.path.exists("dev/dist/"):
                    shutil.rmtree("dev/dist/")
                sys.exit(1)
            
            app_setup = os.path.join(curdir, "dev", "setups")
            executable = os.path.join(app_setup, "%s %s%s" % (QtUtils.definicoes.abreviatura_nome, version, EXE_EXT))
            if not os.path.exists(executable):
                print("Error: Inno setup compile failed, main executable is missing: %s"
                    % executable)
                sys.exit(1)
            # SYSTEMROOT = C:/Windows
            os.system("%s/explorer.exe /n,/e,%s" % (
                os.environ["SYSTEMROOT"], app_setup))


if __name__ == "__main__":
    main()
