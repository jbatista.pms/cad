import csv
from collections import OrderedDict

from loguru import logger
from peewee import *
from PySide2 import QtCore, QtWidgets

from ComumComissoes.lotacao.controles_uteis import LotacaoSelecao
from ComumComissoes.lotacao.models import Lotacao
from QtUtils import subwindowsbase, qt, date
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.db import DataBase
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows

from avaliacao.models import Avaliacao
from servidor.models import Servidor
from servidor.resultado import RelatorioFormularioResultadoFinal
from servidor.servidor import Control as ServidorCadastro
from situacao.models import Situacao
from .templates import estagios_cumpridos


class RelatorioEstagiosCumpridos(subwindowsbase.Listar, LotacaoSelecao):
    classe_ui = estagios_cumpridos.Ui_Form
    acao_tipo = None
    servidores = []
    filtros = OrderedDict()

    def __exportar(self, servidores):
        nome_arquivo = QtWidgets.QFileDialog.getSaveFileName(self,
                "Exportar relatório", '',
                "CSV com cabeçalho (*.csv);;All Files (*)")
        if not (nome_arquivo[0]):
            return

        linhas = [self.__obter_dados_servidor(ser) for ser in servidores]
        if linhas:
            cabecalho = linhas[0].keys()
            arq = open(nome_arquivo[0], 'w')
            arq_csv = csv.DictWriter(arq, fieldnames=cabecalho, delimiter=';', lineterminator='\n')
            arq_csv.writeheader()
            arq_csv.writerows(linhas)
            arq.close()

    def __filtro_adicionar(self, nome, valor):
        self.filtros.update({nome: str(valor)})
    
    def __filtro_em_texto(self):
        if not self.filtros:
            return 'Sem filtros.'
        texto = ''
        for k,v in self.filtros.items():
            if texto:
                texto += '; '
            texto += "{k}: {v}".format(k=k,v=v)
        texto += '.'
        return texto
    
    def __filtro_remover(self, nome):
        if nome in self.filtros:
            self.filtros.pop(nome)
    
    def __obter_avaliacoes(self, servidor):
        return list(Avaliacao.select_todas().where(
            (Avaliacao.servidor==servidor) &
            (Avaliacao.avaliacao==True)
        ))
    
    def __obter_dados_servidor(self, servidor):
        # Obtendo total de conceitos
        return_dict = {
            'cs_insuficiente': 0,
            'cs_regular': 0,
            'cs_bom': 0,
            'cs_otimo': 0,
            'cs_nenhum': 0,
        }
        for av in servidor.avs:
            if av.conceito == av.INSUFICIENTE:
                return_dict['cs_insuficiente'] += 1
            elif av.conceito == av.REGULAR:
                return_dict['cs_regular'] += 1
            elif av.conceito == av.BOM:
                return_dict['cs_bom'] += 1
            elif av.conceito == av.OTIMO:
                return_dict['cs_otimo'] += 1
            else:
                return_dict['cs_nenhum'] += 1
        return_dict.update({
            'nome': servidor.nome,
            'matricula': servidor.matricula,
            'data_admissao': servidor.strftime('data_admissao'),
            'cargo': str(servidor.cargo),
            'lotacao': str(servidor.lotacao_orgao),
            'data_termino_estagio': servidor.avs[-1].strftime('data_final'),
            'total_avs': servidor.total_avs_completas,
        })
        return return_dict

    def __obter_data_base(self):
        data = date.qdate_datetime(self.ui.data.date())
        self.__filtro_adicionar(nome='Data base', valor=data.strftime('%d/%m/%Y'))
        return data
    
    def __obter_lotacao_index(self):
        filtro_nome = 'Lotação'
        lot_ind = self.lotacao_indice()
        if lot_ind > 0:
            lotacao = self.lotacao_selecionada()
            self.__filtro_adicionar(nome=filtro_nome, valor=lotacao)
        else:
            self.__filtro_remover(nome=filtro_nome)
        return lot_ind
    
    def __obter_situacao(self):
        filtro_nome = 'Situação'
        situacao = self.ui.situacao.currentIndex()
        if situacao>0:
            situacao = self.situacoes[situacao-1]
            self.__filtro_adicionar(nome=filtro_nome, valor=situacao.nome)
            logger.debug(f"Situação '{situacao.nome}' selecionada.")
            return situacao
        logger.debug("Situação 'Todos' selecionada.")
        self.__filtro_adicionar(nome=filtro_nome, valor='Todos')
        return None
    
    def __obter_sublotacao_index(self):
        filtro_nome = 'Sublotação'
        sublot_ind = self.sublotacao_indice()
        if sublot_ind > 1:
            self.__filtro_adicionar(nome=filtro_nome, valor=self.sublotacao_selecionada())
        elif sublot_ind > 0:
            self.__filtro_adicionar(nome=filtro_nome, valor='Somente')
        elif sublot_ind == 0:
            self.__filtro_adicionar(nome=filtro_nome, valor='Todas')
        else:
            self.__filtro_remover(nome=filtro_nome)
        return sublot_ind
    
    def __total_avs_completas(self, avaliacoes):
        return len([a for a in avaliacoes if a.conceito > 0 and a.ficha_devolvida])

    def atualizar(self):
        # Query
        servidores = Servidor.select2().join(Avaliacao).filter(
            (Avaliacao.ordem==12) &
            (Avaliacao.data_final<self.__obter_data_base()) &
            (Avaliacao.origem==None) &
            (Servidor.origem==None) &
            (Servidor.estagio_interrompido==False)
        ).group_by(Servidor)

        # Situação
        situacao = self.__obter_situacao()
        if situacao:
            servidores = servidores.filter(Servidor.situacao==situacao)

        # Local de trabalho
        lotacao_index = self.__obter_lotacao_index()
        sublotacao_index = self.__obter_sublotacao_index()
        if lotacao_index:
            if sublotacao_index == 1:
                servidores = servidores.filter(
                    Servidor.lotacao==self.lotacao_selecionada()
                )
            elif sublotacao_index > 0:
                servidores = servidores.filter(
                    Servidor.lotacao==self.sublotacao_selecionada()
                )
            else:
                servidores = servidores.join(Lotacao, on=(Servidor.lotacao==Lotacao.id))
                servidores = servidores.filter(
                    (Lotacao.pai==self.lotacao_selecionada()) |
                    (Servidor.lotacao==self.lotacao_selecionada())
                )
        
        # Obter avaliações e total de avaliações completas
        for ser in servidores:
            ser.avs = self.__obter_avaliacoes(ser)
            ser.total_avs_completas = self.__total_avs_completas(ser.avs)
        
        # Avaliações completas ou incompletas
        if self.ui.av_completa.isChecked():
            servidores = [s for s in servidores if s.total_avs_completas == 6]
            self.__filtro_adicionar(nome='Avaliações', valor='Completas')
        elif self.ui.av_incompleta.isChecked():
            servidores = [s for s in servidores if s.total_avs_completas < 6]
            self.__filtro_adicionar(nome='Avaliações', valor='Incompletas')
        else:
            self.__filtro_remover(nome='Avaliações')

        # Popular tabela
        self.ui.servidores.setRowCount(len(servidores))
        for i,s in enumerate(servidores):
            self.ui.servidores.setVerticalHeaderItem(i,QtWidgets.QTableWidgetItem())
            self.ui.servidores.verticalHeaderItem(i).setText(str(s.id))

            item = qt.QTableWidgetItem()
            item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            item.setCheckState(QtCore.Qt.Unchecked)
            self.ui.servidores.setItem(i,0, item)

            item = qt.QTableWidgetItem()
            item.setText(s.matricula)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.servidores.setItem(i,1, item)

            # Data de admissão
            item = qt.QTableWidgetItem()
            item.setText(s.strftime('data_admissao'))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.servidores.setItem(i,2, item)

            # Data de término do estágio
            item = qt.QTableWidgetItem()
            item.setText(s.avs[-1].strftime('data_final'))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.servidores.setItem(i,3, item)

            # Nome
            item = qt.QTableWidgetItem()
            item.setText(s.nome)
            self.ui.servidores.setItem(i,4, item)

        self.instancias = servidores
        self.servidores = {str(s.id):s for s in servidores}
        self.ui.servidores.resizeColumnsToContents()

    def carregar_situacao(self):
        self.situacoes = Situacao.select2()
        self.ui.situacao.addItem("Todos")
        self.ui.situacao.addItems([s.nome for s in self.situacoes])
    
    def exportar_selecionados(self):
        servidores = self.servidores_selecionados()
        if servidores:
            self.__exportar(servidores)

    def exportar_todos(self):
        self.__exportar(self.instancias)
    
    def imprimir_relatorio(self, modelo):
        template = 'relatorios/documentos/%s.odt' % modelo
        if self.servidores:
            objetos = [{
                'servidores':[self.__obter_dados_servidor(ser) for ser in self.instancias],
                'total': len(self.servidores),
                'filtros': self.__filtro_em_texto(),
            }]
            relatorio = Relatorio(parent=self, objetos=objetos, template=template)
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()
    
    def imprimir_relatorio_avaliacoes(self):
        self.imprimir_relatorio(modelo='estagios_cumpridos_avaliacoes')
    
    def imprimir_relatorio_conceitos(self):
        self.imprimir_relatorio(modelo='estagios_cumpridos_conceitos')
    
    def imprimir_relatorio_cumpridos(self):
        self.imprimir_relatorio(modelo='estagios_cumpridos_datas')
    
    def imprimir_resultados_finais(self):
        servidores = self.servidores_selecionados()
        if servidores:
            relatorio = RelatorioFormularioResultadoFinal(self, objetos=servidores)
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()

    def inicializar(self, *args, **kwargs):
        self.data_atual = DataBase().data()
        self.ui.data.setDate(date.datetime_qdate(self.data_atual))
        self.carregar_lotacoes()
        self.carregar_situacao()
        self.atualizar()
    
    def selecionar_inverter(self):
        for i in range(self.ui.servidores.rowCount()):
            item = self.ui.servidores.item(i,0)
            if item.checkState() == QtCore.Qt.Checked:
                item.setCheckState(QtCore.Qt.Unchecked)
            else:
                item.setCheckState(QtCore.Qt.Checked)

    def servidores_selecionados(self):
        servidores = []
        for i in range(self.ui.servidores.rowCount()):
            if self.ui.servidores.item(i,0).checkState() == QtCore.Qt.Checked:
                servidor = self.servidores[self.ui.servidores.verticalHeaderItem(i).text()]
                servidores.append(servidor)
        if servidores:
            return servidores
        Alerta(self, text='Selecione servidores para impressão!').exec_()
        return None
    
    def selecionar_nenhum(self):
        for i in range(self.ui.servidores.rowCount()):
            self.ui.servidores.item(i,0).setCheckState(QtCore.Qt.Unchecked)

    def selecionar_todos(self):
        for i in range(self.ui.servidores.rowCount()):
            self.ui.servidores.item(i,0).setCheckState(QtCore.Qt.Checked)
    
    def servidor(self, row, col):
        id_s = self.ui.servidores.verticalHeaderItem(row).text()
        if ServidorCadastro(self, instance=self.servidores[id_s]).show():
            self.atualizar()