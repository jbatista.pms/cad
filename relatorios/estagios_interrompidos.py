from PySide2 import QtCore, QtWidgets

from QtUtils import subwindowsbase, qt

from servidor.models import Servidor
from servidor.servidor import Control as ServidorCadastro
from .templates import estagios_interrompidos


class RelatorioEstagiosInterrompidos(subwindowsbase.Listar):
    classe_ui = estagios_interrompidos.Ui_Form
    servidores = []

    def inicializar(self, *args, **kwargs):
        # Query
        servidores = Servidor.select2().filter(estagio_interrompido=True)
        # Popular tabela
        self.ui.servidores.setRowCount(len(servidores))
        for i,s in enumerate(servidores):
            self.ui.servidores.setVerticalHeaderItem(i,QtWidgets.QTableWidgetItem())
            self.ui.servidores.verticalHeaderItem(i).setText(str(s.id))

            item = qt.QTableWidgetItem()
            item.setText(s.matricula)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.servidores.setItem(i,0, item)

            item = qt.QTableWidgetItem()
            item.setText(s.nome)
            self.ui.servidores.setItem(i,1, item)
        self.servidores = {str(s.id):s for s in servidores}
    
    def servidor(self, row, col):
        id_s = self.ui.servidores.verticalHeaderItem(row).text()
        if ServidorCadastro(self, instance=self.servidores[id_s]).show():
            self.atualizar()