# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'estagios_interrompidos.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(558, 434)
        Form.setMinimumSize(QSize(558, 434))
        self.horizontalLayout = QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.frame_2 = QFrame(Form)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame_2)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.servidores = QTableWidget(self.frame_2)
        if (self.servidores.columnCount() < 2):
            self.servidores.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        self.servidores.setObjectName(u"servidores")
        self.servidores.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.servidores.setAlternatingRowColors(True)
        self.servidores.setSelectionMode(QAbstractItemView.SingleSelection)
        self.servidores.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.servidores.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.horizontalHeader().setHighlightSections(False)
        self.servidores.horizontalHeader().setStretchLastSection(True)
        self.servidores.verticalHeader().setVisible(False)
        self.servidores.verticalHeader().setHighlightSections(False)

        self.gridLayout.addWidget(self.servidores, 0, 0, 1, 1)


        self.horizontalLayout.addWidget(self.frame_2)


        self.retranslateUi(Form)
        self.servidores.cellDoubleClicked.connect(Form.servidor)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Est\u00e1gios interrompidos", None))
        ___qtablewidgetitem = self.servidores.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem1 = self.servidores.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Nome", None));
    # retranslateUi

