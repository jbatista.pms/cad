# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'estagios_cumpridos.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(842, 449)
        Form.setMinimumSize(QSize(842, 449))
        self.horizontalLayout = QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setMinimumSize(QSize(250, 0))
        self.frame.setMaximumSize(QSize(250, 16777215))
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame_3 = QFrame(self.frame)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_3)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_4 = QLabel(self.frame_3)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_2.addWidget(self.label_4)

        self.data = QDateEdit(self.frame_3)
        self.data.setObjectName(u"data")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.data.sizePolicy().hasHeightForWidth())
        self.data.setSizePolicy(sizePolicy)
        self.data.setCalendarPopup(True)

        self.horizontalLayout_2.addWidget(self.data)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.verticalLayout.addWidget(self.frame_3)

        self.tabWidget = QTabWidget(self.frame)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout_2 = QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label = QLabel(self.tab)
        self.label.setObjectName(u"label")

        self.verticalLayout_2.addWidget(self.label)

        self.lotacao = QComboBox(self.tab)
        self.lotacao.setObjectName(u"lotacao")

        self.verticalLayout_2.addWidget(self.lotacao)

        self.label_2 = QLabel(self.tab)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_2.addWidget(self.label_2)

        self.sublotacao = QComboBox(self.tab)
        self.sublotacao.setObjectName(u"sublotacao")

        self.verticalLayout_2.addWidget(self.sublotacao)

        self.label_3 = QLabel(self.tab)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_2.addWidget(self.label_3)

        self.situacao = QComboBox(self.tab)
        self.situacao.setObjectName(u"situacao")

        self.verticalLayout_2.addWidget(self.situacao)

        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout_4 = QVBoxLayout(self.groupBox)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.radioButton_3 = QRadioButton(self.groupBox)
        self.radioButton_3.setObjectName(u"radioButton_3")
        self.radioButton_3.setChecked(True)

        self.verticalLayout_4.addWidget(self.radioButton_3)

        self.av_completa = QRadioButton(self.groupBox)
        self.av_completa.setObjectName(u"av_completa")

        self.verticalLayout_4.addWidget(self.av_completa)

        self.av_incompleta = QRadioButton(self.groupBox)
        self.av_incompleta.setObjectName(u"av_incompleta")

        self.verticalLayout_4.addWidget(self.av_incompleta)


        self.verticalLayout_2.addWidget(self.groupBox)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_3)

        self.pushButton = QPushButton(self.tab)
        self.pushButton.setObjectName(u"pushButton")

        self.verticalLayout_2.addWidget(self.pushButton)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_3 = QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.groupBox_2 = QGroupBox(self.tab_2)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.horizontalLayout_3 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pushButton_4 = QPushButton(self.groupBox_2)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout_3.addWidget(self.pushButton_4)

        self.pushButton_6 = QPushButton(self.groupBox_2)
        self.pushButton_6.setObjectName(u"pushButton_6")

        self.horizontalLayout_3.addWidget(self.pushButton_6)

        self.pushButton_5 = QPushButton(self.groupBox_2)
        self.pushButton_5.setObjectName(u"pushButton_5")

        self.horizontalLayout_3.addWidget(self.pushButton_5)


        self.verticalLayout_3.addWidget(self.groupBox_2)

        self.groupBox_3 = QGroupBox(self.tab_2)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.pushButton_9 = QPushButton(self.groupBox_3)
        self.pushButton_9.setObjectName(u"pushButton_9")

        self.verticalLayout_5.addWidget(self.pushButton_9)

        self.label_7 = QLabel(self.groupBox_3)
        self.label_7.setObjectName(u"label_7")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy1)
        self.label_7.setWordWrap(True)

        self.verticalLayout_5.addWidget(self.label_7)

        self.pushButton_3 = QPushButton(self.groupBox_3)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.verticalLayout_5.addWidget(self.pushButton_3)

        self.label_5 = QLabel(self.groupBox_3)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setWordWrap(True)

        self.verticalLayout_5.addWidget(self.label_5)

        self.pushButton_10 = QPushButton(self.groupBox_3)
        self.pushButton_10.setObjectName(u"pushButton_10")

        self.verticalLayout_5.addWidget(self.pushButton_10)

        self.label_6 = QLabel(self.groupBox_3)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setWordWrap(True)

        self.verticalLayout_5.addWidget(self.label_6)

        self.pushButton_2 = QPushButton(self.groupBox_3)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.verticalLayout_5.addWidget(self.pushButton_2)


        self.verticalLayout_3.addWidget(self.groupBox_3)

        self.groupBox_4 = QGroupBox(self.tab_2)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.horizontalLayout_4 = QHBoxLayout(self.groupBox_4)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.pushButton_7 = QPushButton(self.groupBox_4)
        self.pushButton_7.setObjectName(u"pushButton_7")

        self.horizontalLayout_4.addWidget(self.pushButton_7)

        self.pushButton_8 = QPushButton(self.groupBox_4)
        self.pushButton_8.setObjectName(u"pushButton_8")

        self.horizontalLayout_4.addWidget(self.pushButton_8)


        self.verticalLayout_3.addWidget(self.groupBox_4)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.tabWidget.addTab(self.tab_2, "")

        self.verticalLayout.addWidget(self.tabWidget)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout.addWidget(self.frame)

        self.frame_2 = QFrame(Form)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.frame_2)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.servidores = QTableWidget(self.frame_2)
        if (self.servidores.columnCount() < 5):
            self.servidores.setColumnCount(5)
        __qtablewidgetitem = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        self.servidores.setObjectName(u"servidores")
        self.servidores.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.servidores.setAlternatingRowColors(True)
        self.servidores.setSelectionMode(QAbstractItemView.SingleSelection)
        self.servidores.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.servidores.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.horizontalHeader().setHighlightSections(False)
        self.servidores.verticalHeader().setVisible(False)
        self.servidores.verticalHeader().setHighlightSections(False)

        self.gridLayout.addWidget(self.servidores, 0, 0, 1, 1)


        self.horizontalLayout.addWidget(self.frame_2)

        QWidget.setTabOrder(self.data, self.tabWidget)
        QWidget.setTabOrder(self.tabWidget, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.situacao)
        QWidget.setTabOrder(self.situacao, self.radioButton_3)
        QWidget.setTabOrder(self.radioButton_3, self.av_completa)
        QWidget.setTabOrder(self.av_completa, self.av_incompleta)
        QWidget.setTabOrder(self.av_incompleta, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.pushButton_6)
        QWidget.setTabOrder(self.pushButton_6, self.pushButton_5)
        QWidget.setTabOrder(self.pushButton_5, self.pushButton_9)
        QWidget.setTabOrder(self.pushButton_9, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.pushButton_10)
        QWidget.setTabOrder(self.pushButton_10, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton_7)
        QWidget.setTabOrder(self.pushButton_7, self.pushButton_8)
        QWidget.setTabOrder(self.pushButton_8, self.servidores)

        self.retranslateUi(Form)
        self.pushButton.released.connect(Form.atualizar)
        self.servidores.cellDoubleClicked.connect(Form.servidor)
        self.pushButton_2.released.connect(Form.imprimir_resultados_finais)
        self.pushButton_3.released.connect(Form.imprimir_relatorio_avaliacoes)
        self.pushButton_4.released.connect(Form.selecionar_todos)
        self.pushButton_5.released.connect(Form.selecionar_nenhum)
        self.pushButton_6.released.connect(Form.selecionar_inverter)
        self.pushButton_7.released.connect(Form.exportar_todos)
        self.pushButton_8.released.connect(Form.exportar_selecionados)
        self.pushButton_10.released.connect(Form.imprimir_relatorio_conceitos)
        self.pushButton_9.released.connect(Form.imprimir_relatorio_cumpridos)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Est\u00e1gios cumpridos", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Data base:", None))
        self.label.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o:", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Situa\u00e7\u00e3o:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Avalia\u00e7\u00e3o", None))
        self.radioButton_3.setText(QCoreApplication.translate("Form", u"Nenhum", None))
        self.av_completa.setText(QCoreApplication.translate("Form", u"Completa", None))
        self.av_incompleta.setText(QCoreApplication.translate("Form", u"Incompleta", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Pesquisar", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Form", u"Filtros", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Selecionar", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.pushButton_6.setText(QCoreApplication.translate("Form", u"Inverter", None))
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"Nenhum", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"Imprimir", None))
        self.pushButton_9.setText(QCoreApplication.translate("Form", u"Relat\u00f3rio de est\u00e1gios cumpridos", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Relat\u00f3rio simples com o nome de servidores e datas de in\u00edcio e fim.", None))
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"Relat\u00f3rio totalizador de avalia\u00e7\u00f5es", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Lista os est\u00e1gios com totalizador de avalia\u00e7\u00f5es conclu\u00eddas.", None))
        self.pushButton_10.setText(QCoreApplication.translate("Form", u"Relat\u00f3rio totalizador de conceitos", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Lista de est\u00e1gios com totalizador de avalia\u00e7\u00f5es por conveito.", None))
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"Resultados finais", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Form", u"Exportar", None))
        self.pushButton_7.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.pushButton_8.setText(QCoreApplication.translate("Form", u"Selecionados", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Ferramentas", None))
        ___qtablewidgetitem = self.servidores.horizontalHeaderItem(1)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem1 = self.servidores.horizontalHeaderItem(2)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Data de admiss\u00e3o", None));
        ___qtablewidgetitem2 = self.servidores.horizontalHeaderItem(3)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"T\u00e9rmino do est\u00e1gio", None));
        ___qtablewidgetitem3 = self.servidores.horizontalHeaderItem(4)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Nome", None));
    # retranslateUi

