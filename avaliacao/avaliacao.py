from PySide2 import QtCore, QtGui
from datetime import datetime

from QtUtils import subwindowsbase, relatorio
from QtUtils.colecoes import dialogos
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.historico import historico
from QtUtils.text import text_format_uppercase
from QtUtils.validadores import TextoEmMaisculo

from avaliacao import formulario
from avaliacao.colecoes import AVALIACOES, CRITERIOS
from .views.avaliacao import Ui_Dialog as UiDialog1
from .views.avaliacao_2 import Ui_Dialog as UiDialog2
from .utils import conceito, conceito_criterios, conceito_avalicao


class Control(subwindowsbase.Formulario):
    def calcular_conceito(self):
        criterios = []
        for i in CRITERIOS:
            crit = getattr(self.ui, i).text()
            if crit:
                criterios.append(int(crit))
            else:
                break
        self.imprimir_conceito(conceito_criterios(criterios)[1])
    
    def emitir_formulario(self):
        formulario.imprimir(
            avs=[self.instance.dados_para_impressao()],
            duplex=False,
            parent=self,
        )

    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def imprimir_conceito(self, conceito):
        self.ui.conceito.setText(
            ('<html><head/><body><p><span style=" font-size:10pt;'
            'font-weight:600; color:#000000;">%s</span></p></body></html>'
            ) % conceito
            )

    def inicializar(self, *args, **kwargs):
        self.ui.forma_enviada.setValidator(TextoEmMaisculo(self))
        self.ui.forma_devolvida.setValidator(TextoEmMaisculo(self))
        self.ui.motivo.setValidator(TextoEmMaisculo(self))
        self.ui.servidor.setText(self.instance.servidor.nome)
        self.ui.matricula.setText(self.instance.servidor.matricula)
        self.ui.cargo.setText(self.instance.servidor.cargo.nome)
        self.ui.lotacao.setText(str(self.instance.local))
        self.ui.data_inicial.setText(self.instance.strftime('data_inicial'))
        self.ui.data_final.setText(self.instance.strftime('data_final'))
        self.ui.observacao.setPlainText(self.instance.observacao)
        if self.instance.desconsiderar:
            self.ui.desconsiderar.setChecked(True)
            self.ui.motivo.setText(self.instance.desconsiderar_motivo)
        else:
            self.ui.desconsiderar.setChecked(False)
            self.ui.motivo.setText("")
        text_format_uppercase(self.ui.observacao)
        self.ui.nome_formulario.setText(
            AVALIACOES[self.instance.ordem]['descricao'].upper()
        )
        if self.instance.avaliacao:
            for i in CRITERIOS:
                getattr(self.ui, i).setValidator(QtGui.QIntValidator(0,100, self))
            self.ui.informacao.setPlainText(self.instance.informacao)
        hoje = datetime_qdate(datetime.today())
        if self.instance.ficha_enviada:
            self.ui.notificado.setCheckState(QtCore.Qt.Checked)
            self.ui.data_enviada.setDate(datetime_qdate(self.instance.data_enviada))
            self.ui.forma_enviada.setText(self.instance.forma_enviada)
        else:
            self.ui.notificado.setCheckState(QtCore.Qt.Unchecked)
            self.ui.data_enviada.setEnabled(False)
            self.ui.forma_enviada.setEnabled(False)
            self.ui.avaliado.setEnabled(False)
            self.ui.data_enviada.setDate(hoje)
        if self.instance.ficha_devolvida:
            self.ui.avaliado.setCheckState(QtCore.Qt.Checked)
            self.ui.data_devolvida.setDate(datetime_qdate(self.instance.data_devolvida))
            self.ui.forma_devolvida.setText(self.instance.forma_devolvida)
            if self.instance.avaliacao:
                for i in CRITERIOS:
                    getattr(self.ui, i).setEnabled(True)
                    criterio = getattr(self.instance, i)
                    if criterio:
                        getattr(self.ui, i).setText(str(criterio))
                self.imprimir_conceito(conceito(self.instance.conceito))
        else:
            self.ui.avaliado.setCheckState(QtCore.Qt.Unchecked)
            self.ui.data_devolvida.setEnabled(False)
            self.ui.forma_devolvida.setEnabled(False)
            self.ui.data_devolvida.setDate(hoje)
        if self.instance.servidor.data_exclusao is None:
            self.ui.excluido.hide()
        else:
            self.ui.excluido.show()
            self.ui.salvar.setEnabled(False)

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False

    def salvar(self):
        self.instance.observacao = self.ui.observacao.toPlainText().upper()
        self.instance.ficha_enviada = self.ui.notificado.isChecked()
        self.instance.ficha_devolvida = self.ui.avaliado.isChecked()
        self.instance.forma_enviada = self.ui.forma_enviada.text()
        self.instance.forma_devolvida = self.ui.forma_devolvida.text()
        if self.instance.ficha_enviada:
            self.instance.data_enviada = qdate_datetime(self.ui.data_enviada.date())
        else:
            self.instance.data_enviada = None
        if self.instance.ficha_devolvida:
            self.instance.data_devolvida = qdate_datetime(self.ui.data_devolvida.date())
            if self.instance.data_enviada > self.instance.data_devolvida:
                return dialogos.Alerta(self, text="A data de envio é posterior a devolução!").exec_()
        else:
            self.instance.data_devolvida = None
        if self.instance.avaliacao:
            if self.instance.ficha_enviada:
                self.ui.informacao.setEnabled(False)
            if self.instance.ficha_devolvida:
                for i in CRITERIOS:
                    crit = getattr(self.ui,i).text()
                    if crit:
                        setattr(self.instance,i,int(crit))
                    else:
                        return dialogos.Alerta(self,
                            text="Todos os critérios devem estar preenchidos!"
                            ).exec_()
                        setattr(self.instance,i,0)
                self.instance.conceito = conceito_avalicao(self.instance)[0]
            else:
                for i in CRITERIOS:
                    setattr(self.instance,i,0)
                self.instance.conceito = 0
                self.instance.informacao = self.ui.informacao.toPlainText().upper()
        
        if self.ui.desconsiderar.isChecked():
            self.instance.desconsiderar = True
            self.instance.desconsiderar_motivo = self.ui.motivo.text()
        else:
            self.instance.desconsiderar = False
            self.instance.desconsiderar_motivo = ""
        self.instance.save()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()

    def obter_classe_ui(self, *args, **kwargs):
        if self.instance.avaliacao:
            return UiDialog2()
        return UiDialog1()
