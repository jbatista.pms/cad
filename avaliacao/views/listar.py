# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(932, 523)
        Form.setMinimumSize(QSize(932, 523))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.mensagem = QTabWidget(Form)
        self.mensagem.setObjectName(u"mensagem")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mensagem.sizePolicy().hasHeightForWidth())
        self.mensagem.setSizePolicy(sizePolicy)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_2 = QGridLayout(self.tab)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.groupBox_3 = QGroupBox(self.tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.groupBox_3.sizePolicy().hasHeightForWidth())
        self.groupBox_3.setSizePolicy(sizePolicy1)
        self.gridLayout_5 = QGridLayout(self.groupBox_3)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.acompanhamento = QRadioButton(self.groupBox_3)
        self.acompanhamento.setObjectName(u"acompanhamento")

        self.gridLayout_5.addWidget(self.acompanhamento, 1, 0, 1, 1)

        self.formulario_todos = QRadioButton(self.groupBox_3)
        self.formulario_todos.setObjectName(u"formulario_todos")
        self.formulario_todos.setChecked(True)

        self.gridLayout_5.addWidget(self.formulario_todos, 2, 0, 1, 1)

        self.avaliacao = QRadioButton(self.groupBox_3)
        self.avaliacao.setObjectName(u"avaliacao")

        self.gridLayout_5.addWidget(self.avaliacao, 0, 0, 1, 1)


        self.gridLayout_2.addWidget(self.groupBox_3, 3, 0, 1, 2)

        self.groupBox_4 = QGroupBox(self.tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy1.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy1)
        self.gridLayout_6 = QGridLayout(self.groupBox_4)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.lotacao = QComboBox(self.groupBox_4)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy2 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy2)
        self.lotacao.setMinimumSize(QSize(250, 0))

        self.gridLayout_6.addWidget(self.lotacao, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox_4)
        self.label.setObjectName(u"label")

        self.gridLayout_6.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox_4)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_6.addWidget(self.label_2, 2, 0, 1, 1)

        self.sublotacao = QComboBox(self.groupBox_4)
        self.sublotacao.setObjectName(u"sublotacao")
        sizePolicy2.setHeightForWidth(self.sublotacao.sizePolicy().hasHeightForWidth())
        self.sublotacao.setSizePolicy(sizePolicy2)

        self.gridLayout_6.addWidget(self.sublotacao, 3, 0, 1, 1)


        self.gridLayout_2.addWidget(self.groupBox_4, 4, 0, 1, 2)

        self.groupBox_6 = QGroupBox(self.tab)
        self.groupBox_6.setObjectName(u"groupBox_6")
        sizePolicy1.setHeightForWidth(self.groupBox_6.sizePolicy().hasHeightForWidth())
        self.groupBox_6.setSizePolicy(sizePolicy1)
        self.gridLayout_3 = QGridLayout(self.groupBox_6)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.atrasadas = QRadioButton(self.groupBox_6)
        self.atrasadas.setObjectName(u"atrasadas")

        self.gridLayout_3.addWidget(self.atrasadas, 2, 0, 1, 1)

        self.em_aberto = QRadioButton(self.groupBox_6)
        self.em_aberto.setObjectName(u"em_aberto")
        self.em_aberto.setChecked(True)

        self.gridLayout_3.addWidget(self.em_aberto, 0, 0, 1, 1)

        self.iniciadas = QRadioButton(self.groupBox_6)
        self.iniciadas.setObjectName(u"iniciadas")

        self.gridLayout_3.addWidget(self.iniciadas, 0, 1, 1, 1)

        self.estado_todos = QRadioButton(self.groupBox_6)
        self.estado_todos.setObjectName(u"estado_todos")

        self.gridLayout_3.addWidget(self.estado_todos, 3, 1, 1, 1)

        self.concluidas = QRadioButton(self.groupBox_6)
        self.concluidas.setObjectName(u"concluidas")

        self.gridLayout_3.addWidget(self.concluidas, 3, 0, 1, 1)

        self.apurar = QRadioButton(self.groupBox_6)
        self.apurar.setObjectName(u"apurar")
        self.apurar.setEnabled(True)

        self.gridLayout_3.addWidget(self.apurar, 2, 1, 1, 1)


        self.gridLayout_2.addWidget(self.groupBox_6, 1, 0, 1, 2)

        self.label_5 = QLabel(self.tab)
        self.label_5.setObjectName(u"label_5")
        sizePolicy3 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy3)

        self.gridLayout_2.addWidget(self.label_5, 0, 0, 1, 1)

        self.data_base = QDateEdit(self.tab)
        self.data_base.setObjectName(u"data_base")
        sizePolicy4 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.data_base.sizePolicy().hasHeightForWidth())
        self.data_base.setSizePolicy(sizePolicy4)
        self.data_base.setCalendarPopup(True)

        self.gridLayout_2.addWidget(self.data_base, 0, 1, 1, 1)

        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        self.formLayout = QFormLayout(self.groupBox)
        self.formLayout.setObjectName(u"formLayout")
        self.label_8 = QLabel(self.groupBox)
        self.label_8.setObjectName(u"label_8")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_8)

        self.situacao = QComboBox(self.groupBox)
        self.situacao.setObjectName(u"situacao")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.situacao)


        self.gridLayout_2.addWidget(self.groupBox, 5, 0, 1, 2)

        self.mensagem.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_2 = QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_7 = QLabel(self.tab_2)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label_7)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 0, -1, -1)
        self.selecionar_todos = QPushButton(self.tab_2)
        self.selecionar_todos.setObjectName(u"selecionar_todos")

        self.horizontalLayout.addWidget(self.selecionar_todos)

        self.deselecionar_todos = QPushButton(self.tab_2)
        self.deselecionar_todos.setObjectName(u"deselecionar_todos")

        self.horizontalLayout.addWidget(self.deselecionar_todos)

        self.inverter_selecao = QPushButton(self.tab_2)
        self.inverter_selecao.setObjectName(u"inverter_selecao")

        self.horizontalLayout.addWidget(self.inverter_selecao)


        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer)

        self.exportar_filtradas = QPushButton(self.tab_2)
        self.exportar_filtradas.setObjectName(u"exportar_filtradas")
        self.exportar_filtradas.setEnabled(True)
        sizePolicy4.setHeightForWidth(self.exportar_filtradas.sizePolicy().hasHeightForWidth())
        self.exportar_filtradas.setSizePolicy(sizePolicy4)
        self.exportar_filtradas.setLayoutDirection(Qt.LeftToRight)

        self.verticalLayout_2.addWidget(self.exportar_filtradas)

        self.label_4 = QLabel(self.tab_2)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_4)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_2)

        self.criar_formulario = QPushButton(self.tab_2)
        self.criar_formulario.setObjectName(u"criar_formulario")

        self.verticalLayout_2.addWidget(self.criar_formulario)

        self.duplex = QCheckBox(self.tab_2)
        self.duplex.setObjectName(u"duplex")

        self.verticalLayout_2.addWidget(self.duplex)

        self.label_6 = QLabel(self.tab_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_6)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_5)

        self.informacoes = QPushButton(self.tab_2)
        self.informacoes.setObjectName(u"informacoes")

        self.verticalLayout_2.addWidget(self.informacoes)

        self.label_3 = QLabel(self.tab_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_3)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_3)

        self.acao = QPushButton(self.tab_2)
        self.acao.setObjectName(u"acao")
        self.acao.setEnabled(True)

        self.verticalLayout_2.addWidget(self.acao)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.mensagem.addTab(self.tab_2, "")

        self.gridLayout.addWidget(self.mensagem, 0, 0, 1, 2)

        self.avaliacoes = QTableWidget(Form)
        if (self.avaliacoes.columnCount() < 8):
            self.avaliacoes.setColumnCount(8)
        __qtablewidgetitem = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(5, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(6, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(7, __qtablewidgetitem7)
        self.avaliacoes.setObjectName(u"avaliacoes")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.avaliacoes.sizePolicy().hasHeightForWidth())
        self.avaliacoes.setSizePolicy(sizePolicy5)
        self.avaliacoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.avaliacoes.setAlternatingRowColors(True)
        self.avaliacoes.setSelectionMode(QAbstractItemView.SingleSelection)
        self.avaliacoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.avaliacoes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setSortingEnabled(True)
        self.avaliacoes.horizontalHeader().setHighlightSections(False)
        self.avaliacoes.horizontalHeader().setProperty("showSortIndicator", True)
        self.avaliacoes.horizontalHeader().setStretchLastSection(False)
        self.avaliacoes.verticalHeader().setVisible(False)

        self.gridLayout.addWidget(self.avaliacoes, 0, 2, 4, 1)

        self.filtrar = QPushButton(Form)
        self.filtrar.setObjectName(u"filtrar")

        self.gridLayout.addWidget(self.filtrar, 1, 0, 1, 2)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer_2, 3, 0, 1, 2)

        self.total_avaliacoes = QLabel(Form)
        self.total_avaliacoes.setObjectName(u"total_avaliacoes")
        sizePolicy4.setHeightForWidth(self.total_avaliacoes.sizePolicy().hasHeightForWidth())
        self.total_avaliacoes.setSizePolicy(sizePolicy4)

        self.gridLayout.addWidget(self.total_avaliacoes, 2, 0, 1, 2)

        QWidget.setTabOrder(self.mensagem, self.data_base)
        QWidget.setTabOrder(self.data_base, self.em_aberto)
        QWidget.setTabOrder(self.em_aberto, self.iniciadas)
        QWidget.setTabOrder(self.iniciadas, self.atrasadas)
        QWidget.setTabOrder(self.atrasadas, self.apurar)
        QWidget.setTabOrder(self.apurar, self.concluidas)
        QWidget.setTabOrder(self.concluidas, self.estado_todos)
        QWidget.setTabOrder(self.estado_todos, self.avaliacao)
        QWidget.setTabOrder(self.avaliacao, self.acompanhamento)
        QWidget.setTabOrder(self.acompanhamento, self.formulario_todos)
        QWidget.setTabOrder(self.formulario_todos, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.selecionar_todos)
        QWidget.setTabOrder(self.selecionar_todos, self.deselecionar_todos)
        QWidget.setTabOrder(self.deselecionar_todos, self.inverter_selecao)
        QWidget.setTabOrder(self.inverter_selecao, self.exportar_filtradas)
        QWidget.setTabOrder(self.exportar_filtradas, self.criar_formulario)
        QWidget.setTabOrder(self.criar_formulario, self.duplex)
        QWidget.setTabOrder(self.duplex, self.informacoes)
        QWidget.setTabOrder(self.informacoes, self.acao)
        QWidget.setTabOrder(self.acao, self.avaliacoes)

        self.retranslateUi(Form)
        self.filtrar.released.connect(Form.atualizar)
        self.exportar_filtradas.released.connect(Form.exportar)
        self.avaliacoes.cellDoubleClicked.connect(Form.abrir_avaliacao)
        self.lotacao.currentIndexChanged.connect(Form.carregar_sublotacao)
        self.selecionar_todos.pressed.connect(Form.selecionar_todos)
        self.deselecionar_todos.released.connect(Form.deselecionar_todos)
        self.inverter_selecao.released.connect(Form.reverter_selecao)
        self.acao.released.connect(Form.acao)
        self.criar_formulario.released.connect(Form.criar_formulario)
        self.informacoes.released.connect(Form.adicionar_informacao)

        self.mensagem.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Avalia\u00e7\u00f5es", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"Formul\u00e1rio:", None))
        self.acompanhamento.setText(QCoreApplication.translate("Form", u"De acompanhamento", None))
        self.formulario_todos.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.avaliacao.setText(QCoreApplication.translate("Form", u"De avalia\u00e7\u00e3o", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Form", u"Local de trabalho:", None))
        self.label.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("Form", u"Estado do formul\u00e1rio:", None))
        self.atrasadas.setText(QCoreApplication.translate("Form", u"Atrasado", None))
        self.em_aberto.setText(QCoreApplication.translate("Form", u"Enviar", None))
        self.iniciadas.setText(QCoreApplication.translate("Form", u"Enviado", None))
        self.estado_todos.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.concluidas.setText(QCoreApplication.translate("Form", u"Finalizado", None))
        self.apurar.setText(QCoreApplication.translate("Form", u"Apurar", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Data base:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Servidor", None))
        self.label_8.setText(QCoreApplication.translate("Form", u"Situa\u00e7\u00e3o:", None))
        self.mensagem.setTabText(self.mensagem.indexOf(self.tab), QCoreApplication.translate("Form", u"Filtros", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Sele\u00e7\u00e3o:", None))
        self.selecionar_todos.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.deselecionar_todos.setText(QCoreApplication.translate("Form", u"Nenhum", None))
        self.inverter_selecao.setText(QCoreApplication.translate("Form", u"Inverter", None))
        self.exportar_filtradas.setText(QCoreApplication.translate("Form", u"Exportar avalia\u00e7\u00f5es relacionadas", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"<html><head/><body><p>Exporte as avalia\u00e7\u00f5es filtradas para um arquivo edit\u00e1vel.</p></body></html>", None))
        self.criar_formulario.setText(QCoreApplication.translate("Form", u"Criar formul\u00e1rios", None))
        self.duplex.setText(QCoreApplication.translate("Form", u"Impress\u00e3o duplex.", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"<html><head/><body><p align=\"justify\">Incluir uma p\u00e1gina em branco ap\u00f3s cada formul\u00e1rio, para impress\u00e3o em frente e verso.</p></body></html>", None))
        self.informacoes.setText(QCoreApplication.translate("Form", u"Adiconar informa\u00e7\u00f5es", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"<html><head/><body><p align=\"justify\">Adicione informa\u00e7\u00f5es que julgar relevantes as avalia\u00e7\u00f5es selecionadas.</p></body></html>", None))
        self.acao.setText(QCoreApplication.translate("Form", u"Enviar formul\u00e1rios", None))
        self.mensagem.setTabText(self.mensagem.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Ferramentas", None))
        ___qtablewidgetitem = self.avaliacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"id", None));
        ___qtablewidgetitem1 = self.avaliacoes.horizontalHeaderItem(2)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Servidor", None));
        ___qtablewidgetitem2 = self.avaliacoes.horizontalHeaderItem(3)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Avalia\u00e7\u00e3o", None));
        ___qtablewidgetitem3 = self.avaliacoes.horizontalHeaderItem(4)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem4 = self.avaliacoes.horizontalHeaderItem(5)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Form", u"Cargo", None));
        ___qtablewidgetitem5 = self.avaliacoes.horizontalHeaderItem(6)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None));
        ___qtablewidgetitem6 = self.avaliacoes.horizontalHeaderItem(7)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o", None));
        self.filtrar.setText(QCoreApplication.translate("Form", u"Filtrar", None))
        self.total_avaliacoes.setText(QCoreApplication.translate("Form", u"0 avalia\u00e7\u00f5es encontradas", None))
    # retranslateUi

