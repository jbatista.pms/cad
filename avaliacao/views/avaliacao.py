# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'avaliacao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(433, 575)
        Dialog.setMinimumSize(QSize(433, 575))
        Dialog.setMaximumSize(QSize(433, 575))
        self.gridLayout_3 = QGridLayout(Dialog)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.observacao = QTextEdit(Dialog)
        self.observacao.setObjectName(u"observacao")
        self.observacao.setTabChangesFocus(True)

        self.gridLayout_3.addWidget(self.observacao, 5, 0, 1, 2)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label, 4, 0, 1, 2)

        self.frame_2 = QFrame(Dialog)
        self.frame_2.setObjectName(u"frame_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy1)
        self.frame_2.setLayoutDirection(Qt.RightToLeft)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)

        self.historico = QPushButton(self.frame_2)
        self.historico.setObjectName(u"historico")

        self.horizontalLayout_2.addWidget(self.historico)

        self.salvar = QPushButton(self.frame_2)
        self.salvar.setObjectName(u"salvar")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy2)

        self.horizontalLayout_2.addWidget(self.salvar)

        self.fechar = QPushButton(self.frame_2)
        self.fechar.setObjectName(u"fechar")
        sizePolicy2.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy2)

        self.horizontalLayout_2.addWidget(self.fechar)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.gridLayout_3.addWidget(self.frame_2, 7, 0, 1, 2)

        self.groupBox_2 = QGroupBox(Dialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy)
        self.gridLayout_4 = QGridLayout(self.groupBox_2)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_9 = QLabel(self.groupBox_2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_4.addWidget(self.label_9, 1, 0, 1, 1)

        self.notificado = QCheckBox(self.groupBox_2)
        self.notificado.setObjectName(u"notificado")

        self.gridLayout_4.addWidget(self.notificado, 0, 0, 1, 2)

        self.forma_enviada = QLineEdit(self.groupBox_2)
        self.forma_enviada.setObjectName(u"forma_enviada")

        self.gridLayout_4.addWidget(self.forma_enviada, 4, 0, 1, 2)

        self.avaliado = QCheckBox(self.groupBox_2)
        self.avaliado.setObjectName(u"avaliado")

        self.gridLayout_4.addWidget(self.avaliado, 0, 2, 1, 2)

        self.label_10 = QLabel(self.groupBox_2)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_4.addWidget(self.label_10, 1, 2, 1, 1)

        self.forma_devolvida = QLineEdit(self.groupBox_2)
        self.forma_devolvida.setObjectName(u"forma_devolvida")

        self.gridLayout_4.addWidget(self.forma_devolvida, 4, 2, 1, 2)

        self.data_enviada = QDateEdit(self.groupBox_2)
        self.data_enviada.setObjectName(u"data_enviada")
        self.data_enviada.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.data_enviada, 2, 0, 1, 1)

        self.data_devolvida = QDateEdit(self.groupBox_2)
        self.data_devolvida.setObjectName(u"data_devolvida")
        self.data_devolvida.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.data_devolvida, 2, 2, 1, 1)

        self.label_11 = QLabel(self.groupBox_2)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_4.addWidget(self.label_11, 3, 0, 1, 2)

        self.label_12 = QLabel(self.groupBox_2)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout_4.addWidget(self.label_12, 3, 2, 1, 2)


        self.gridLayout_3.addWidget(self.groupBox_2, 3, 0, 1, 2)

        self.groupBox = QGroupBox(Dialog)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy3)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.data_inicial = QLabel(self.groupBox)
        self.data_inicial.setObjectName(u"data_inicial")
        sizePolicy4 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.data_inicial.sizePolicy().hasHeightForWidth())
        self.data_inicial.setSizePolicy(sizePolicy4)

        self.gridLayout.addWidget(self.data_inicial, 0, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")
        sizePolicy3.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy3)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 0, 3, 1, 1)

        self.data_final = QLabel(self.groupBox)
        self.data_final.setObjectName(u"data_final")
        sizePolicy4.setHeightForWidth(self.data_final.sizePolicy().hasHeightForWidth())
        self.data_final.setSizePolicy(sizePolicy4)

        self.gridLayout.addWidget(self.data_final, 0, 4, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox, 2, 0, 1, 2)

        self.excluido = QLabel(Dialog)
        self.excluido.setObjectName(u"excluido")
        self.excluido.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.excluido, 6, 0, 1, 2)

        self.groupBox_3 = QGroupBox(Dialog)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.formLayout = QFormLayout(self.groupBox_3)
        self.formLayout.setObjectName(u"formLayout")
        self.desconsiderar = QCheckBox(self.groupBox_3)
        self.desconsiderar.setObjectName(u"desconsiderar")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.desconsiderar)

        self.motivo = QLineEdit(self.groupBox_3)
        self.motivo.setObjectName(u"motivo")
        self.motivo.setEnabled(False)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.motivo)


        self.gridLayout_3.addWidget(self.groupBox_3, 1, 0, 1, 2)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.formLayout_2 = QFormLayout(self.frame)
#ifndef Q_OS_MAC
        self.formLayout_2.setContentsMargins(9, 9, 9, 9)
#endif
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.formLayout_2.setLabelAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.label_7 = QLabel(self.frame)
        self.label_7.setObjectName(u"label_7")
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_7)

        self.servidor = QLabel(self.frame)
        self.servidor.setObjectName(u"servidor")
        sizePolicy5 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy5)

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.servidor)

        self.label_6 = QLabel(self.frame)
        self.label_6.setObjectName(u"label_6")
        sizePolicy.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy)
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_6)

        self.matricula = QLabel(self.frame)
        self.matricula.setObjectName(u"matricula")
        sizePolicy1.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy1)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.matricula)

        self.label_5 = QLabel(self.frame)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_5)

        self.cargo = QLabel(self.frame)
        self.cargo.setObjectName(u"cargo")
        sizePolicy1.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy1)

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.cargo)

        self.label_8 = QLabel(self.frame)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.label_8)

        self.lotacao = QLabel(self.frame)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy1.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy1)

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.lotacao)

        self.label_4 = QLabel(self.frame)
        self.label_4.setObjectName(u"label_4")

        self.formLayout_2.setWidget(4, QFormLayout.LabelRole, self.label_4)

        self.nome_formulario = QLabel(self.frame)
        self.nome_formulario.setObjectName(u"nome_formulario")
        sizePolicy1.setHeightForWidth(self.nome_formulario.sizePolicy().hasHeightForWidth())
        self.nome_formulario.setSizePolicy(sizePolicy1)

        self.formLayout_2.setWidget(4, QFormLayout.FieldRole, self.nome_formulario)


        self.gridLayout_3.addWidget(self.frame, 0, 0, 1, 2)

        QWidget.setTabOrder(self.desconsiderar, self.motivo)
        QWidget.setTabOrder(self.motivo, self.notificado)
        QWidget.setTabOrder(self.notificado, self.data_enviada)
        QWidget.setTabOrder(self.data_enviada, self.forma_enviada)
        QWidget.setTabOrder(self.forma_enviada, self.avaliado)
        QWidget.setTabOrder(self.avaliado, self.data_devolvida)
        QWidget.setTabOrder(self.data_devolvida, self.forma_devolvida)
        QWidget.setTabOrder(self.forma_devolvida, self.observacao)
        QWidget.setTabOrder(self.observacao, self.fechar)
        QWidget.setTabOrder(self.fechar, self.salvar)
        QWidget.setTabOrder(self.salvar, self.historico)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.close)
        self.salvar.released.connect(Dialog.salvar)
        self.notificado.toggled.connect(self.data_enviada.setEnabled)
        self.notificado.toggled.connect(self.avaliado.setEnabled)
        self.avaliado.toggled.connect(self.data_devolvida.setEnabled)
        self.avaliado.toggled.connect(self.notificado.setDisabled)
        self.avaliado.toggled.connect(self.data_enviada.setDisabled)
        self.avaliado.toggled.connect(self.forma_enviada.setDisabled)
        self.avaliado.toggled.connect(self.forma_devolvida.setEnabled)
        self.notificado.toggled.connect(self.forma_enviada.setEnabled)
        self.historico.released.connect(Dialog.historico)
        self.desconsiderar.toggled.connect(self.motivo.setEnabled)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Avalia\u00e7\u00e3o", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Observa\u00e7\u00f5es:", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Estado", None))
        self.label_9.setText(QCoreApplication.translate("Dialog", u"Data:", None))
        self.notificado.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio enviado", None))
        self.avaliado.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio devolvido", None))
        self.label_10.setText(QCoreApplication.translate("Dialog", u"Data:", None))
        self.label_11.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.label_12.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Per\u00edodo de avalia\u00e7\u00e3o", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Data inicial:", None))
        self.data_inicial.setText(QCoreApplication.translate("Dialog", u"data_inicial", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Data final:", None))
        self.data_final.setText(QCoreApplication.translate("Dialog", u"data_final", None))
        self.excluido.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0000;\">Servidor exclu\u00eddo</span></p></body></html>", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Dialog", u"Desconsiderar avalia\u00e7\u00e3o", None))
        self.desconsiderar.setText(QCoreApplication.translate("Dialog", u"Motivo:", None))
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"servidor", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Cargo:", None))
        self.cargo.setText(QCoreApplication.translate("Dialog", u"cargo", None))
        self.label_8.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.lotacao.setText(QCoreApplication.translate("Dialog", u"lotacao", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio:", None))
        self.nome_formulario.setText(QCoreApplication.translate("Dialog", u"nome", None))
    # retranslateUi

