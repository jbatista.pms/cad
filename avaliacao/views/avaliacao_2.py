# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'avaliacao_2.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(695, 546)
        Dialog.setMinimumSize(QSize(695, 546))
        Dialog.setMaximumSize(QSize(695, 546))
        self.gridLayout_3 = QGridLayout(Dialog)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(-1, -1, 5, -1)
        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, -1, -1)
        self.groupBox_3 = QGroupBox(Dialog)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.formLayout_2 = QFormLayout(self.groupBox_3)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.formLayout_2.setLabelAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.label_13 = QLabel(self.groupBox_3)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_13)

        self.assiduidade = QLineEdit(self.groupBox_3)
        self.assiduidade.setObjectName(u"assiduidade")
        self.assiduidade.setEnabled(False)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.assiduidade.sizePolicy().hasHeightForWidth())
        self.assiduidade.setSizePolicy(sizePolicy)
        self.assiduidade.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.assiduidade)

        self.label_14 = QLabel(self.groupBox_3)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(1, QFormLayout.LabelRole, self.label_14)

        self.disciplina = QLineEdit(self.groupBox_3)
        self.disciplina.setObjectName(u"disciplina")
        self.disciplina.setEnabled(False)
        sizePolicy.setHeightForWidth(self.disciplina.sizePolicy().hasHeightForWidth())
        self.disciplina.setSizePolicy(sizePolicy)
        self.disciplina.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(1, QFormLayout.FieldRole, self.disciplina)

        self.label_15 = QLabel(self.groupBox_3)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(2, QFormLayout.LabelRole, self.label_15)

        self.iniciativa = QLineEdit(self.groupBox_3)
        self.iniciativa.setObjectName(u"iniciativa")
        self.iniciativa.setEnabled(False)
        sizePolicy.setHeightForWidth(self.iniciativa.sizePolicy().hasHeightForWidth())
        self.iniciativa.setSizePolicy(sizePolicy)
        self.iniciativa.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(2, QFormLayout.FieldRole, self.iniciativa)

        self.label_16 = QLabel(self.groupBox_3)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(3, QFormLayout.LabelRole, self.label_16)

        self.produtividade = QLineEdit(self.groupBox_3)
        self.produtividade.setObjectName(u"produtividade")
        self.produtividade.setEnabled(False)
        sizePolicy.setHeightForWidth(self.produtividade.sizePolicy().hasHeightForWidth())
        self.produtividade.setSizePolicy(sizePolicy)
        self.produtividade.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(3, QFormLayout.FieldRole, self.produtividade)

        self.label_17 = QLabel(self.groupBox_3)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(4, QFormLayout.LabelRole, self.label_17)

        self.responsabilidade = QLineEdit(self.groupBox_3)
        self.responsabilidade.setObjectName(u"responsabilidade")
        self.responsabilidade.setEnabled(False)
        sizePolicy.setHeightForWidth(self.responsabilidade.sizePolicy().hasHeightForWidth())
        self.responsabilidade.setSizePolicy(sizePolicy)
        self.responsabilidade.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(4, QFormLayout.FieldRole, self.responsabilidade)

        self.label_18 = QLabel(self.groupBox_3)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout_2.setWidget(5, QFormLayout.LabelRole, self.label_18)

        self.conceito = QLabel(self.groupBox_3)
        self.conceito.setObjectName(u"conceito")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.conceito.sizePolicy().hasHeightForWidth())
        self.conceito.setSizePolicy(sizePolicy1)
        self.conceito.setStyleSheet(u"")
        self.conceito.setFrameShape(QFrame.NoFrame)
        self.conceito.setFrameShadow(QFrame.Plain)
        self.conceito.setTextFormat(Qt.RichText)
        self.conceito.setAlignment(Qt.AlignCenter)

        self.formLayout_2.setWidget(5, QFormLayout.FieldRole, self.conceito)


        self.verticalLayout_4.addWidget(self.groupBox_3)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.label)

        self.observacao = QTextEdit(Dialog)
        self.observacao.setObjectName(u"observacao")
        sizePolicy2 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.observacao.sizePolicy().hasHeightForWidth())
        self.observacao.setSizePolicy(sizePolicy2)
        self.observacao.setTabChangesFocus(True)

        self.verticalLayout_4.addWidget(self.observacao)


        self.gridLayout_3.addLayout(self.verticalLayout_4, 0, 2, 1, 1)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.frame_3 = QFrame(self.frame)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.formLayout = QFormLayout(self.frame_3)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setLabelAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.label_7 = QLabel(self.frame_3)
        self.label_7.setObjectName(u"label_7")
        sizePolicy1.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy1)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_7)

        self.servidor = QLabel(self.frame_3)
        self.servidor.setObjectName(u"servidor")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy3)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.servidor)

        self.label_5 = QLabel(self.frame_3)
        self.label_5.setObjectName(u"label_5")
        sizePolicy1.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy1)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_5)

        self.cargo = QLabel(self.frame_3)
        self.cargo.setObjectName(u"cargo")
        sizePolicy3.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy3)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.cargo)

        self.label_8 = QLabel(self.frame_3)
        self.label_8.setObjectName(u"label_8")
        sizePolicy1.setHeightForWidth(self.label_8.sizePolicy().hasHeightForWidth())
        self.label_8.setSizePolicy(sizePolicy1)
        self.label_8.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_8)

        self.lotacao = QLabel(self.frame_3)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy3.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy3)

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.lotacao)

        self.label_4 = QLabel(self.frame_3)
        self.label_4.setObjectName(u"label_4")
        sizePolicy1.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy1)

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_4)

        self.nome_formulario = QLabel(self.frame_3)
        self.nome_formulario.setObjectName(u"nome_formulario")
        sizePolicy3.setHeightForWidth(self.nome_formulario.sizePolicy().hasHeightForWidth())
        self.nome_formulario.setSizePolicy(sizePolicy3)

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.nome_formulario)

        self.label_6 = QLabel(self.frame_3)
        self.label_6.setObjectName(u"label_6")
        sizePolicy1.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy1)
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_6)

        self.matricula = QLabel(self.frame_3)
        self.matricula.setObjectName(u"matricula")
        sizePolicy3.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy3)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.matricula)


        self.verticalLayout.addWidget(self.frame_3)

        self.groupBox_4 = QGroupBox(self.frame)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy)
        self.formLayout_3 = QFormLayout(self.groupBox_4)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.desconsiderar = QCheckBox(self.groupBox_4)
        self.desconsiderar.setObjectName(u"desconsiderar")

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole, self.desconsiderar)

        self.motivo = QLineEdit(self.groupBox_4)
        self.motivo.setObjectName(u"motivo")
        self.motivo.setEnabled(False)

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole, self.motivo)


        self.verticalLayout.addWidget(self.groupBox_4)

        self.groupBox = QGroupBox(self.frame)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")
        sizePolicy4 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy4)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 0, 3, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        sizePolicy4.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy4)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.data_inicial = QLabel(self.groupBox)
        self.data_inicial.setObjectName(u"data_inicial")
        sizePolicy5 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.data_inicial.sizePolicy().hasHeightForWidth())
        self.data_inicial.setSizePolicy(sizePolicy5)

        self.gridLayout.addWidget(self.data_inicial, 0, 1, 1, 1)

        self.data_final = QLabel(self.groupBox)
        self.data_final.setObjectName(u"data_final")
        sizePolicy5.setHeightForWidth(self.data_final.sizePolicy().hasHeightForWidth())
        self.data_final.setSizePolicy(sizePolicy5)

        self.gridLayout.addWidget(self.data_final, 0, 4, 1, 1)


        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(self.frame)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy)
        self.gridLayout_4 = QGridLayout(self.groupBox_2)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.data_enviada = QDateEdit(self.groupBox_2)
        self.data_enviada.setObjectName(u"data_enviada")
        self.data_enviada.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.data_enviada, 2, 0, 1, 1)

        self.label_12 = QLabel(self.groupBox_2)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout_4.addWidget(self.label_12, 3, 2, 1, 2)

        self.notificado = QCheckBox(self.groupBox_2)
        self.notificado.setObjectName(u"notificado")

        self.gridLayout_4.addWidget(self.notificado, 0, 0, 1, 2)

        self.label_11 = QLabel(self.groupBox_2)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_4.addWidget(self.label_11, 3, 0, 1, 2)

        self.label_9 = QLabel(self.groupBox_2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_4.addWidget(self.label_9, 1, 0, 1, 1)

        self.data_devolvida = QDateEdit(self.groupBox_2)
        self.data_devolvida.setObjectName(u"data_devolvida")
        self.data_devolvida.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.data_devolvida, 2, 2, 1, 1)

        self.label_10 = QLabel(self.groupBox_2)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_4.addWidget(self.label_10, 1, 2, 1, 1)

        self.forma_devolvida = QLineEdit(self.groupBox_2)
        self.forma_devolvida.setObjectName(u"forma_devolvida")

        self.gridLayout_4.addWidget(self.forma_devolvida, 4, 2, 1, 2)

        self.avaliado = QCheckBox(self.groupBox_2)
        self.avaliado.setObjectName(u"avaliado")

        self.gridLayout_4.addWidget(self.avaliado, 0, 2, 1, 2)

        self.forma_enviada = QLineEdit(self.groupBox_2)
        self.forma_enviada.setObjectName(u"forma_enviada")

        self.gridLayout_4.addWidget(self.forma_enviada, 4, 0, 1, 2)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.groupBox_5 = QGroupBox(self.frame)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.gridLayout_2 = QGridLayout(self.groupBox_5)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.informacao = QTextEdit(self.groupBox_5)
        self.informacao.setObjectName(u"informacao")
        self.informacao.setTabChangesFocus(True)

        self.gridLayout_2.addWidget(self.informacao, 0, 0, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_5)


        self.gridLayout_3.addWidget(self.frame, 0, 0, 1, 1)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        sizePolicy6 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy6)
        self.gridLayout_5 = QGridLayout(self.widget)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setObjectName(u"gridLayout_5")

        self.gridLayout_3.addWidget(self.widget, 0, 3, 2, 1)

        self.frame_2 = QFrame(Dialog)
        self.frame_2.setObjectName(u"frame_2")
        sizePolicy3.setHeightForWidth(self.frame_2.sizePolicy().hasHeightForWidth())
        self.frame_2.setSizePolicy(sizePolicy3)
        self.frame_2.setLayoutDirection(Qt.RightToLeft)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.salvar = QPushButton(self.frame_2)
        self.salvar.setObjectName(u"salvar")
        sizePolicy7 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy7)

        self.horizontalLayout_2.addWidget(self.salvar)

        self.pushButton = QPushButton(self.frame_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout_2.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self.frame_2)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.fechar = QPushButton(self.frame_2)
        self.fechar.setObjectName(u"fechar")
        sizePolicy7.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy7)

        self.horizontalLayout_2.addWidget(self.fechar)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.gridLayout_3.addWidget(self.frame_2, 2, 0, 1, 3)

        self.excluido = QLabel(Dialog)
        self.excluido.setObjectName(u"excluido")
        self.excluido.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.excluido, 1, 0, 1, 3)

        QWidget.setTabOrder(self.desconsiderar, self.motivo)
        QWidget.setTabOrder(self.motivo, self.notificado)
        QWidget.setTabOrder(self.notificado, self.data_enviada)
        QWidget.setTabOrder(self.data_enviada, self.forma_enviada)
        QWidget.setTabOrder(self.forma_enviada, self.avaliado)
        QWidget.setTabOrder(self.avaliado, self.data_devolvida)
        QWidget.setTabOrder(self.data_devolvida, self.forma_devolvida)
        QWidget.setTabOrder(self.forma_devolvida, self.assiduidade)
        QWidget.setTabOrder(self.assiduidade, self.informacao)
        QWidget.setTabOrder(self.informacao, self.disciplina)
        QWidget.setTabOrder(self.disciplina, self.iniciativa)
        QWidget.setTabOrder(self.iniciativa, self.produtividade)
        QWidget.setTabOrder(self.produtividade, self.responsabilidade)
        QWidget.setTabOrder(self.responsabilidade, self.observacao)
        QWidget.setTabOrder(self.observacao, self.fechar)
        QWidget.setTabOrder(self.fechar, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.salvar)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.close)
        self.salvar.released.connect(Dialog.salvar)
        self.notificado.toggled.connect(self.data_enviada.setEnabled)
        self.notificado.toggled.connect(self.avaliado.setEnabled)
        self.avaliado.toggled.connect(self.data_devolvida.setEnabled)
        self.avaliado.toggled.connect(self.notificado.setDisabled)
        self.avaliado.toggled.connect(self.data_enviada.setDisabled)
        self.avaliado.toggled.connect(self.forma_enviada.setDisabled)
        self.avaliado.toggled.connect(self.forma_devolvida.setEnabled)
        self.notificado.toggled.connect(self.forma_enviada.setEnabled)
        self.avaliado.toggled.connect(self.assiduidade.setEnabled)
        self.avaliado.toggled.connect(self.disciplina.setEnabled)
        self.avaliado.toggled.connect(self.iniciativa.setEnabled)
        self.avaliado.toggled.connect(self.produtividade.setEnabled)
        self.avaliado.toggled.connect(self.responsabilidade.setEnabled)
        self.assiduidade.editingFinished.connect(Dialog.calcular_conceito)
        self.disciplina.editingFinished.connect(Dialog.calcular_conceito)
        self.iniciativa.editingFinished.connect(Dialog.calcular_conceito)
        self.produtividade.editingFinished.connect(Dialog.calcular_conceito)
        self.responsabilidade.editingFinished.connect(Dialog.calcular_conceito)
        self.pushButton.released.connect(Dialog.historico)
        self.pushButton_2.released.connect(Dialog.emitir_formulario)
        self.desconsiderar.toggled.connect(self.motivo.setEnabled)
        self.notificado.toggled.connect(self.informacao.setDisabled)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Avalia\u00e7\u00e3o", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Dialog", u"Crit\u00e9rios", None))
        self.label_13.setText(QCoreApplication.translate("Dialog", u"Assiduidade:", None))
        self.label_14.setText(QCoreApplication.translate("Dialog", u"Disciplina:", None))
        self.label_15.setText(QCoreApplication.translate("Dialog", u"Iniciativa:", None))
        self.label_16.setText(QCoreApplication.translate("Dialog", u"Produtividade:", None))
        self.label_17.setText(QCoreApplication.translate("Dialog", u"Responsabilidade:", None))
        self.label_18.setText(QCoreApplication.translate("Dialog", u"Conceito:", None))
        self.conceito.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">Nenhum</span></p></body></html>", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Observa\u00e7\u00f5es:", None))
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"servidor", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Cargo:", None))
        self.cargo.setText(QCoreApplication.translate("Dialog", u"cargo", None))
        self.label_8.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.lotacao.setText(QCoreApplication.translate("Dialog", u"lotacao", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio:", None))
        self.nome_formulario.setText(QCoreApplication.translate("Dialog", u"nome", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Dialog", u"Desconsiderar avalia\u00e7\u00e3o", None))
        self.desconsiderar.setText(QCoreApplication.translate("Dialog", u"Motivo:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Per\u00edodo de avalia\u00e7\u00e3o", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Data final:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Data inicial:", None))
        self.data_inicial.setText(QCoreApplication.translate("Dialog", u"data_inicial", None))
        self.data_final.setText(QCoreApplication.translate("Dialog", u"data_final", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Estado", None))
        self.label_12.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.notificado.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio enviado", None))
        self.label_11.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.label_9.setText(QCoreApplication.translate("Dialog", u"Data:", None))
        self.label_10.setText(QCoreApplication.translate("Dialog", u"Data:", None))
        self.avaliado.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio devolvido", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00f5es complementares (incluir no formul\u00e1rio)", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.excluido.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p align=\"center\"><span style=\" color:#ff0000;\">Servidor exclu\u00eddo</span></p></body></html>", None))
    # retranslateUi

