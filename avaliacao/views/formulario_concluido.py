# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'formulario_concluido.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(400, 134)
        Dialog.setMinimumSize(QSize(400, 134))
        Dialog.setMaximumSize(QSize(400, 134))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.abrir = QPushButton(Dialog)
        self.abrir.setObjectName(u"abrir")

        self.gridLayout.addWidget(self.abrir, 1, 0, 1, 1)

        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)


        self.retranslateUi(Dialog)
        self.abrir.released.connect(Dialog.abrir)
        self.pushButton.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Concluido!", None))
        self.abrir.setText(QCoreApplication.translate("Dialog", u"Abrir formul\u00e1rios", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rios criados!", None))
    # retranslateUi

