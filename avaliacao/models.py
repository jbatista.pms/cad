from peewee import *

from QtUtils import text
from QtUtils.historico.models import Historico

from servidor.models import LocalDeTrabalho, Servidor
from .colecoes import AVALIACOES


class Avaliacao(Historico):
    NENHUM = 0
    INSUFICIENTE = 1
    REGULAR = 2
    BOM = 3
    OTIMO = 4

    CONCEITOS = (
        (NENHUM, 'Nenhum'),
        (INSUFICIENTE, 'Insuficiente'),
        (REGULAR, 'Regular'),
        (BOM, 'Bom'),
        (OTIMO, 'Ótimo')
    )

    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    local = ForeignKeyField(LocalDeTrabalho, related_name='avaliacoes')
    servidor = ForeignKeyField(
        Servidor,
        related_name='avaliacoes',
        on_delete='CASCADE',
        )
    ordem = IntegerField()
    data_inicial = DateField()
    data_final = DateField()
    data_enviada = DateField(null=True)
    data_devolvida = DateField(null=True)
    data_base = DateField()
    ficha_enviada = BooleanField(default=False)
    ficha_devolvida = BooleanField(default=False)
    forma_enviada = CharField(default="", null=True)
    forma_devolvida = CharField(default="", null=True)
    avaliacao = BooleanField(default=False)
    anterior = ForeignKeyField(
        'self',
        related_name='posterior',
        null=True,
        on_delete='SET NULL',
        )
    observacao = TextField(null=True, default='')
    informacao = TextField(null=True, default='')
    indeterminado = BooleanField(default=False)
    desconsiderar = BooleanField(default=False)
    desconsiderar_motivo = CharField(default="", null=True)

    # Critérios de avaliação
    assiduidade = IntegerField(default=0, null=True)
    disciplina = IntegerField(default=0, null=True)
    iniciativa = IntegerField(default=0, null=True)
    produtividade = IntegerField(default=0, null=True)
    responsabilidade = IntegerField(default=0, null=True)
    conceito = IntegerField(default=0, null=True, choices=CONCEITOS)

    def __str__(self):
        return AVALIACOES[self.ordem]['nome']
    
    @classmethod
    def __select(cls, *args, **kwargs):
        return cls.select(*args, **kwargs).order_by(cls.data_inicial, cls.ordem)
    
    def dados_para_impressao(self):
        from evento.models import Evento
        eventos_query = Evento.select2().where(
            (Evento.servidor==self.servidor) &
            (Evento.data_inicial>=self.data_inicial) &
            (Evento.data_inicial<=self.data_final)
        )
        eventos = []
        if sum([ev.total for ev in eventos_query]) > 30:
            for i in eventos_query:
                eventos.append('- {}\n'.format(i.evento_str()))
        return {
            'quebrar': True,
            'data_admissao': self.servidor.data_admissao.strftime("%d/%m/%Y"),
            'data_inicial': self.data_inicial.strftime("%d/%m/%Y"),
            'data_final': self.data_final.strftime("%d/%m/%Y"),
            'matricula': self.servidor.matricula,
            'nome': self.servidor.nome,
            'cargo': self.servidor.cargo.nome,
            'lotacao_descricao': self.local.lotacao.descricao,
            'descricao_abreviada': AVALIACOES[self.ordem]['abreviacao'],
            'descricao_completa': AVALIACOES[self.ordem]['descricao'].upper(),
            'descricao_nome': AVALIACOES[self.ordem]['nome'].upper(),
            'lotacao': text.text_p_nome(str(self.local.lotacao.pai or self.local.lotacao)),
            'eventos': eventos,
            'informacao': self.informacao,
        }
    
    def descricao(self):
        return str(self)
    
    @staticmethod
    def exportar_extras():
        return ['servidor', 'local', 'servidor.cargo', 'descricao']
    
    @staticmethod
    def exportar_ignorar():
        return ['ordem','data_base'] + Historico.exportar_ignorar()

    @staticmethod
    def ignorar_campos_mudancas():
        return [
            'ordem',
            'avaliacao',
            'data_base',
            'servidor',
            'anterior'
        ] + Historico.ignorar_campos_mudancas()
    
    @property
    def nota(self):
        return sum([
            self.assiduidade or 0, 
            self.disciplina or 0, 
            self.iniciativa or 0, 
            self.produtividade or 0, 
            self.responsabilidade or 0,
        ])//5
    
    @property
    def obter_conceito(self):
        if self.conceito:
            return dict(self.CONCEITOS)[self.conceito]
        return dict(self.CONCEITOS)[0]
    
    @classmethod
    def select2(cls, excluidos=False, indeterminado=False, *args, **kwargs):
        return cls.__select(*args, **kwargs).join(Servidor).where(
            (cls.origem.is_null(True)) &
            (cls.indeterminado==indeterminado) &
            (Servidor.data_exclusao.is_null(not excluidos))
        )
    
    @classmethod
    def select_todas(cls, *args, **kwargs):
        return cls.__select(*args, **kwargs).where(cls.origem.is_null(True))


from . import signals