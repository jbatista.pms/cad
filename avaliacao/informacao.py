from QtUtils import subwindowsbase
from QtUtils.db import DataBase
from QtUtils.text import text_format_uppercase

from .views.adicionar_informacao import Ui_Dialog


class Informacao(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def accept(self):
        observacao = self.ui.observacao.toPlainText()
        with DataBase().obter_database().atomic():
            for av in self.instance:
                if av.observacao is None:
                    av.observacao = observacao
                else:
                    av.observacao += '\n'+observacao
                av.save()
        return super().accept()

    def inicializar(self, *args, **kwargs):
        text_format_uppercase(self.ui.observacao)