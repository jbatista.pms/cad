import csv
from PySide2 import QtWidgets

from QtUtils.colecoes.dialogos import Alerta

from avaliacao.models import Avaliacao
from servidor.models import Servidor

def exportar_avaliacoes(parent, servidores=Servidor.select2()):
    cabecalho = [
        'servidor__nome',
        'servidor__matricula',
        'servidor__cargo',
        'servidor__lotacao',
        'servidor__situacao',
        'avaliacao__formulario',
        'avaliacao__local',
        'avaliacao__data_inicial',
        'avaliacao__data_final',
        'avaliacao__data_enviada',
        'avaliacao__data_devolvida',
        'avaliacao__forma_envio',
        'avaliacao__forma_devolucao',
        'avaliacao__desconsiderar',
        'avaliacao__desconsiderar_motivo',
        'avaliacao__nota_assiduidade',
        'avaliacao__nota_disciplina',
        'avaliacao__nota_iniciativa',
        'avaliacao__nota_produtividade',
        'avaliacao__nota_responsabilidade',
        'avaliacao__conceito',
    ]

    # Selecionar arquivo destino
    arq_nome = QtWidgets.QFileDialog.getSaveFileName(parent,
            "Exportar avaliações", '',
            "CSV com cabeçalho (*.csv);;All Files (*)")
    if not arq_nome[0]:
        return

    # Preparar arquivo
    try:    
        arq = open(arq_nome[0], 'w')
    except PermissionError as a:
        return Alerta(
            parent,
            text=(
                "Não foi possível salvar!\n\n"
                "O arquivo está aberto em outro programa ou sendo utilizado por algum processo.\n"
            )
        ).exec_()
    arq_csv = csv.DictWriter(
        arq, 
        fieldnames=cabecalho, 
        delimiter=';', 
        lineterminator='\n'
    )
    arq_csv.writeheader()

    # Popular linhas
    linhas = []
    for ser in servidores:
        for av in Avaliacao.select2().filter((Avaliacao.servidor==ser)&(Avaliacao.avaliacao==True)):
            result = {
                'servidor__nome': ser.nome,
                'servidor__matricula': ser.matricula,
                'servidor__cargo': ser.cargo.nome,
                'servidor__lotacao': ser.lotacao.nome,
                'servidor__situacao': ser.situacao.nome,
                'avaliacao__formulario': str(av),
                'avaliacao__local': str(av.local),
                'avaliacao__data_inicial': av.data_inicial,
                'avaliacao__data_final': av.data_final,
                'avaliacao__data_enviada': av.data_enviada,
                'avaliacao__data_devolvida': av.data_devolvida,
                'avaliacao__forma_envio': av.forma_enviada,
                'avaliacao__forma_devolucao': av.forma_devolvida,
                'avaliacao__nota_assiduidade': av.assiduidade if av.ficha_devolvida else '',
                'avaliacao__nota_disciplina': av.disciplina if av.ficha_devolvida else '',
                'avaliacao__nota_iniciativa': av.iniciativa if av.ficha_devolvida else '',
                'avaliacao__nota_produtividade': av.produtividade if av.ficha_devolvida else '',
                'avaliacao__nota_responsabilidade': av.responsabilidade if av.ficha_devolvida else '',
                'avaliacao__conceito': av.obter_conceito,
            }
            linhas.append(result)

    # Finalizar e salvar arquivo
    arq_csv.writerows(linhas)
    arq.close()