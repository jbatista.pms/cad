import csv
from datetime import date, timedelta
from PySide2 import QtCore, QtWidgets

from ComumComissoes.lotacao.controles_uteis import LotacaoSelecao
from ComumComissoes.lotacao.models import Lotacao
from QtUtils import subwindowsbase, qt
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.text import text_p_nome
from loguru import logger

from avaliacao import avaliacao, formulario
from avaliacao.colecoes import AVALIACOES
from avaliacao.models import Avaliacao
from evento.models import Evento
from servidor.models import LocalDeTrabalho, Servidor
from situacao.models import Situacao

from . import formulario
from .acao import Acao
from .colecoes import ACAO_ENVIAR, ACAO_RECEBER
from .informacao import Informacao
from .views.listar import Ui_Form


class Control(subwindowsbase.Listar, LotacaoSelecao):
    classe_ui = Ui_Form
    acao_tipo = None

    def acao(self):
        if self.acao_tipo != None:
            if Acao(
                    parent=self, 
                    acao=self.acao_tipo, 
                    instance=self.avaliacoes_selecionadas()
                ).exec_():
                self.atualizar()
    
    def adicionar_informacao(self):
        if Informacao(self, instance=self.avaliacoes_selecionadas()).exec_():
            self.atualizar()
        
    def carregar_situacoes(self):
        self.situacoes = list(Situacao.select2())
        self.ui.situacao.addItem("Todos")
        self.ui.situacao.addItems([s.nome for s in self.situacoes])

    def inicializar(self, *args, **kwargs):
        self.carregar_lotacoes()
        self.carregar_situacoes()
        self.ui.data_base.setDate(datetime_qdate(date.today()))
        headerH = self.ui.avaliacoes.horizontalHeader()
        headerH.setSectionHidden(0,True)
        headerH.setDefaultAlignment(QtCore.Qt.AlignCenter)

    def listar(self):
        avaliacoes = Avaliacao.select2()

        if not self.ui.estado_todos.isChecked():
            data = qdate_datetime(self.ui.data_base.date())
            if self.ui.em_aberto.isChecked():
                avaliacoes = avaliacoes.filter(
                    (Avaliacao.ficha_enviada==False) &
                    (Avaliacao.data_final<data)
                    )
            elif self.ui.iniciadas.isChecked():
                avaliacoes = avaliacoes.filter(
                    (Avaliacao.ficha_enviada==True) &
                    (Avaliacao.ficha_devolvida==False) &
                    (Avaliacao.data_final<data)
                    )
            elif self.ui.atrasadas.isChecked():
                data -= timedelta(days=60)
                avaliacoes = avaliacoes.filter(
                    (Avaliacao.ficha_enviada==True) &
                    (Avaliacao.ficha_devolvida==False) &
                    (Avaliacao.data_enviada<data)
                    )
            elif self.ui.apurar.isChecked():
                avaliacoes = avaliacoes.filter(
                    (Avaliacao.ficha_devolvida==True) &
                    (Avaliacao.data_final<=data) &
                    (Avaliacao.avaliacao==True) &
                    (Avaliacao.conceito==Avaliacao.NENHUM)
                    )
            elif self.ui.concluidas.isChecked():
                avaliacoes = avaliacoes.filter(
                    (Avaliacao.ficha_devolvida==True) &
                    (Avaliacao.data_final<=data) &
                    (
                        ((Avaliacao.conceito!=Avaliacao.NENHUM) & (Avaliacao.avaliacao==True)) |
                        (Avaliacao.avaliacao==False)
                    )
                    )

        if not self.ui.formulario_todos.isChecked():
            avaliacoes = avaliacoes.filter(
                Avaliacao.avaliacao==self.ui.avaliacao.isChecked()
                )

        lotacao_index = self.lotacao_indice()
        sublotacao_index = self.sublotacao_indice()
        if lotacao_index:
            avaliacoes = avaliacoes.join(LocalDeTrabalho, on=(Avaliacao.local==LocalDeTrabalho.id))
            if sublotacao_index == 1:
                avaliacoes = avaliacoes.filter(
                    LocalDeTrabalho.lotacao==self.lotacao_selecionada()
                )
            elif sublotacao_index > 0:
                avaliacoes = avaliacoes.filter(
                    LocalDeTrabalho.lotacao==self.sublotacao_selecionada()
                )
            else:
                avaliacoes = avaliacoes.join(Lotacao)
                avaliacoes = avaliacoes.filter(
                    (Lotacao.pai==self.lotacao_selecionada()) |
                    (LocalDeTrabalho.lotacao==self.lotacao_selecionada())
                )
            avaliacoes = avaliacoes.group_by(Avaliacao)

        # Situação
        if self.ui.situacao.currentIndex()>0:
            avaliacoes = avaliacoes.filter(
                Servidor.situacao==self.situacoes[self.ui.situacao.currentIndex()-1]
            )

        avaliacoes = avaliacoes.filter(Avaliacao.desconsiderar==False).objects()
        self.lista_dict = {str(a.id):a for a in avaliacoes}
        return avaliacoes

    def atualizar(self, *args, **kwargs):
        self.determinar_acao()
        avaliacoes = self.listar()
        self.avaliacoes_dicionario = dict([(i.id,i) for i in avaliacoes])
        avaliacoes_count = len(avaliacoes)
        self.ui.total_avaliacoes.setText(
            "%i avaliações encontradas" % avaliacoes_count
            )
        self.ui.avaliacoes.setSortingEnabled(False)
        self.ui.avaliacoes.setRowCount(avaliacoes_count)
        for f in range(avaliacoes_count):
            av = avaliacoes[f]

            item = qt.QTableWidgetItem()
            item.setText(str(av.id))
            self.ui.avaliacoes.setItem(f,0, item)

            item = qt.QTableWidgetItem()
            item.setFlags(
                QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled
                )
            item.setCheckState(QtCore.Qt.Unchecked)
            item.setSizeHint(QtCore.QSize(0, 0))
            self.ui.avaliacoes.setItem(f,1, item)

            item = qt.QTableWidgetItem()
            item.setText(av.strftime('data_final'))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.avaliacoes.setItem(f,4, item)

            # Cargo
            item = qt.QTableWidgetItem()
            item.setText(av.servidor.cargo.nome)
            self.ui.avaliacoes.setItem(f,5, item)

            item = qt.QTableWidgetItem()
            if av.local.lotacao.pai is None:
                item.setText(av.local.lotacao.nome)
                self.ui.avaliacoes.setItem(f,6, item)
                item = qt.QTableWidgetItem()
                item.setText("")
                self.ui.avaliacoes.setItem(f,7, item)
            else:
                item.setText(av.local.lotacao.pai.nome)
                self.ui.avaliacoes.setItem(f,6, item)
                item = qt.QTableWidgetItem()
                item.setText(av.local.lotacao.nome)
                self.ui.avaliacoes.setItem(f,7, item)


            item = qt.QTableWidgetItem()
            item.setText(
                AVALIACOES[av.ordem]['descricao']
                )
            self.ui.avaliacoes.setItem(f,3, item)

            item = qt.QTableWidgetItem()
            item.setText(av.servidor.nome)
            self.ui.avaliacoes.setItem(f,2, item)
        self.ui.avaliacoes.setSortingEnabled(True)
        self.ui.avaliacoes.resizeColumnsToContents()

    def avaliacoes_selecionadas(self, somente=False):
        avaliacoes = []
        for i in range(self.ui.avaliacoes.rowCount()):
            if self.ui.avaliacoes.item(i,1).checkState() == QtCore.Qt.Checked:
                av = self.avaliacoes_dicionario[
                        int(self.ui.avaliacoes.item(i,0).text())
                        ]
                if av.avaliacao or not somente:
                    avaliacoes.append(av)
        if avaliacoes:
            return avaliacoes
        Alerta(self, text='Não há avaliações selecionadas').exec_()
        return None

    def criar_formulario(self):
        formulario.imprimir(
            avs=[
                av.dados_para_impressao() 
                for av in self.avaliacoes_selecionadas(somente=True)
            ],
            duplex=self.ui.duplex.isChecked(),
            sub_janela=True,
            parent=self,
        )
    
    def determinar_acao(self):
        if self.ui.em_aberto.isChecked():
            self.acao_tipo = ACAO_ENVIAR
            self.ui.acao.show()
            self.ui.acao.setText("Enviar formulários")
        elif self.ui.iniciadas.isChecked() or self.ui.atrasadas.isChecked():
            self.acao_tipo = ACAO_RECEBER
            self.ui.acao.show()
            self.ui.acao.setText("Receber formulários")
        else:
            self.acao_tipo = None
            self.ui.acao.hide()

    def exportar(self):
        nome_arquivo = QtWidgets.QFileDialog.getSaveFileName(self,
                "Exportar relatório", '',
                "CSV com cabeçalho (*.csv);;All Files (*)")
        avaliacoes = self.listar()
        if not (nome_arquivo[0] and avaliacoes):
            return

        arq = open(nome_arquivo[0], 'w')
        c_servidor = ['nome','matricula','data_admissao','cargo']
        c_avaliacao = ['data_inicial','data_final','observacao','lotacao', 'sub_lotacao']
        c_AVALIACAO = ['nome_avaliacao','descricao','abreviacao']
        para_nomes = ('nome','cargo','lotacao','sub_lotacao')
        evento_str = '{descricao} - Entre {data_i} e {data_f}{inf}\n'
        cabecalho = c_servidor + c_avaliacao + c_AVALIACAO + ['eventos',]
        arq_csv = csv.DictWriter(
            arq, fieldnames=cabecalho, delimiter=';', lineterminator='\n'
            )
        arq_csv.writeheader()
        linhas = []
        for av in avaliacoes:
            registro = {}
            for i in c_servidor:
                registro[i] = str(getattr(av.servidor, i))
            registro['data_admissao'] = av.servidor.strftime('data_admissao')
            if av.local.lotacao.pai:
                registro['lotacao'] = str(av.local.lotacao.pai)
                registro['sub_lotacao'] = str(av.local.lotacao)
            else:
                registro['lotacao'] = str(av.local.lotacao)
                registro['sub_lotacao'] = ''
            registro['data_inicial'] = av.strftime('data_inicial')
            registro['data_final'] = av.strftime('data_final')
            registro['observacao'] = av.observacao
            registro['nome_avaliacao'] = AVALIACOES[av.ordem]['nome']
            for i in c_AVALIACAO[1:]:
                registro[i] = AVALIACOES[av.ordem][i]
            # Para nomes
            for i in para_nomes:
                registro[i] = text_p_nome(registro[i])
            # Eventos
            eventos = av.servidor.eventos.select().where(
                (Evento.data_inicial>=av.data_inicial) &
                (Evento.data_inicial<=av.data_final)
            )
            registro['eventos'] = ''
            for i in eventos:
                registro['eventos'] += evento_str.format(
                    descricao=i.descricao,
                    data_i=i.strftime('data_inicial'),
                    data_f=i.strftime('data_final'),
                    inf=': ' + i.informacoes if i.informacoes else ''
                    )
            linhas.append(registro)
        arq_csv.writerows(linhas)
        arq.close()

    def abrir_avaliacao(self, row, column):
        id_av = self.ui.avaliacoes.item(row,0).text()
        instance = self.lista_dict[id_av]
        if avaliacao.Control(self, instance=instance).exec_():
            self.atualizar()

    def selecionar_todos(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            self.ui.avaliacoes.item(i,1).setCheckState(QtCore.Qt.Checked)

    def deselecionar_todos(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            self.ui.avaliacoes.item(i,1).setCheckState(QtCore.Qt.Unchecked)
    
    def reverter_selecao(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            item = self.ui.avaliacoes.item(i,1)
            if item.checkState() == QtCore.Qt.Checked:
                item.setCheckState(QtCore.Qt.Unchecked)
            else:
                item.setCheckState(QtCore.Qt.Checked)
