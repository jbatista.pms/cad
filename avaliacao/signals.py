from playhouse.signals import post_save

from QtUtils.db import DataBase

from servidor.models import Servidor
from .models import Avaliacao
from .utils import estimar_datas

@post_save(sender=Servidor)
def criar_avaliacoes(model_class, instance, created):
    if created:
        anterior = None
        with DataBase().obter_database().atomic():
            for i in range(1,12,2):
                facompanhamento = Avaliacao(
                    servidor=instance,
                    ordem = i,
                    data_base = instance.data_admissao,
                    anterior = anterior,
                    avaliacao = False,
                    local=instance.local
                )
                estimar_datas(facompanhamento)
                facompanhamento.save()
                favaliacao = Avaliacao(
                    servidor=instance,
                    ordem = i+1,
                    data_base = instance.data_admissao,
                    anterior = anterior,
                    avaliacao = True,
                    local=instance.local
                )
                estimar_datas(favaliacao)
                favaliacao.save()

                anterior = favaliacao