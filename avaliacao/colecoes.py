
ROMANOS = [0, 'I', 'II', 'III', 'IV', 'V', 'VI']
AVALIACOES = {}

cont = 1
for i in range(2,13,2):
    ordem = {
        'nome': "Formulário %s" % ROMANOS[cont],
        'descricao': "%iª Avaliação" % cont,
        'abreviacao': "%iª AV" % cont,
    }
    AVALIACOES[i] = ordem
    cont += 1

cont = 1
for i in range(1,12,2):
    ordem = {
        'nome': "Ficha de acompanhamento %s" % ROMANOS[cont],
        'descricao': "%iª Ficha de acompanhamento" % cont,
        'abreviacao': "%iª FA" % cont,
    }
    AVALIACOES[i] = ordem
    cont += 1


CRITERIOS = (
	'assiduidade',
	'disciplina',
	'iniciativa',
	'produtividade',
	'responsabilidade',
	)

ACAO_ENVIAR = 1
ACAO_RECEBER = 2