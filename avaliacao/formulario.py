from loguru import logger
from QtUtils import relatorio

def imprimir(avs: list, duplex: bool, sub_janela: bool= False, parent=None):
    avs[-1]['quebrar'] = False
    logger.debug(f"Imprimir relatório com os argumentos: duplex={duplex}")
    rel_obj = relatorio.Relatorio(
        parent=parent,
        objetos=[{'forms': avs}],
        template='avaliacao/templates/formulario.odt',
        duplex=duplex,
    )
    rel_obj.exec_()
    if rel_obj.documento:
        if sub_janela:
            relatorio.RelatorioViewSubWindows(
                parent=parent,
                relatorio=rel_obj.documento,
            ).show()
        else:
            relatorio.RelatorioViewDialog(
                parent=parent,
                relatorio=rel_obj.documento,
            ).show()