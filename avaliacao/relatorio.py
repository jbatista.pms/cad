from QtUtils.relatorio.dados import ModeloRelatorio
from .colecoes import AVALIACOES
from .models import Avaliacao


class AvaliacaoModeloRelatorio(ModeloRelatorio):
    modelo = Avaliacao
    funcoes = {
        'descricao': lambda av: AVALIACOES[av.ordem]['descricao'],
        'conceito': lambda av: av.obter_conceito,
        'nota': lambda av: av.nota,
        'data_enviada': lambda av: av.data_enviada if av.ficha_enviada else '-----',
        'data_devolvida': lambda av: av.data_devolvida if av.ficha_devolvida else '-----',
        'observacao': lambda av: av.observacao.replace('\n', '\r') if av.observacao else 'Sem informação.'
    }