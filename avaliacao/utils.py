from datetime import timedelta

from evento.models import Evento
from prazos import Inicio

from .colecoes import CRITERIOS
from .models import Avaliacao

def estimar_datas(avaliacao):
    if avaliacao.ficha_enviada or avaliacao.ficha_devolvida or avaliacao.origem != None:
        return

    # Avaliações levam 6 meses, e fichas de acompanhamentos 3 meses
    intervalo = 6 if avaliacao.avaliacao else 3
    # Determinar data inicial e final da avaliação
    if avaliacao.anterior:
        avaliacao.data_base = avaliacao.anterior.data_base
        dia_base = avaliacao.anterior.data_base.day
        # Avaliação se inicia um dia após a anterior
        data_inicial = avaliacao.anterior.data_final + timedelta(days=1)
    else:
        avaliacao.data_base = data_inicial = avaliacao.servidor.data_admissao
        dia_base = avaliacao.servidor.data_admissao.day

    data_final = Inicio(data_inicial).prazo(
        intervalo=intervalo, dia_base=dia_base, coincidir=False
        )

    # Postergar período de avaliação com base em eventos
    def f_eventos(serv, dinicial, dfinal):
        return Evento.select2().where(
            (Evento.servidor==serv) & 
            (Evento.data_inicial>=dinicial) & 
            (Evento.data_inicial<=dfinal)
        ).order_by(Evento.data_inicial)
    eventos = f_eventos(avaliacao.servidor, data_inicial, data_final)
    if eventos:
        total = 0
        dataf_filtro = data_final
        while eventos:
            dataf_filtro = data_final
            for ev in eventos:
                total += ev.total
            if total > 30:
                datai_filtro = ev.data_inicial + timedelta(days=1)
                dataf_filtro += timedelta(days=total) 
                eventos = f_eventos(avaliacao.servidor, datai_filtro, dataf_filtro)
            else:
                eventos = None
        data_final = dataf_filtro
        avaliacao.data_base = data_final + timedelta(days=1)

    # Determinar se prazo final é indeterminado com base em eventos
    avaliacao.indeterminado = bool(
        Evento.select2().where(
            (Evento.servidor==avaliacao.servidor) &
            (Evento.data_inicial<=avaliacao.data_final) &
            (Evento.indeterminado==True)
        )
    )
    avaliacao.data_inicial = data_inicial
    avaliacao.data_final = data_final

def estimar_datas_servidor(servidor):
    for av in Avaliacao.select_todas().where(Avaliacao.servidor==servidor).objects():
        estimar_datas(av)
        if av.ficha_enviada==False:
            av.local = servidor.lotacao_na_data(av.data_final)
        av.save()
    servidor.estagio_interrompido = bool(
        Avaliacao.select_todas().where(
            (Avaliacao.servidor==servidor) &
            (Avaliacao.indeterminado==True)
        )
    )
    servidor.save()

def verificar_datas(servidor):
    data = None
    for i in Avaliacao.select_todas().where(Avaliacao.servidor==servidor).objects():
        if not data:
            if i.avaliacao:
                data = i.data_final + timedelta(days=1)
            continue
        if not data == i.data_inicial:
            return str(i)
        if i.avaliacao:
            data = i.data_final + timedelta(days=1)
    return False

def conceito_avalicao(avaliacao):
    return conceito_criterios([getattr(avaliacao, i) for i in CRITERIOS])

def conceito_criterios(lista):
    if len(lista) == 5:
        return conceito_soma(sum(lista))
    else:
        return 0, 'Nenhum'

def conceito_soma(soma):
    soma = soma/5
    conceitos = dict(Avaliacao.CONCEITOS)
    if soma > 85:
        return Avaliacao.OTIMO, conceitos[Avaliacao.OTIMO]
    elif soma > 69:
        return Avaliacao.BOM, conceitos[Avaliacao.BOM]
    elif soma > 34:
        return Avaliacao.REGULAR, conceitos[Avaliacao.REGULAR]
    else:
        return Avaliacao.INSUFICIENTE, conceitos[Avaliacao.INSUFICIENTE]

def conceito(conceito):
    if conceito == 4:
        return 'Ótimo'
    elif conceito == 1:
        return 'Insuficiente'
    elif conceito == 2:
        return 'Regular'
    elif conceito == 3:
        return 'Bom'
    else:
        return 'Nenhum'
