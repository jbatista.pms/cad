from datetime import date

from QtUtils import subwindowsbase
from QtUtils.db import DataBase
from QtUtils.date import datetime_qdate, qdate_datetime
from QtUtils.text import text_format_uppercase
from QtUtils.validadores import TextoEmMaisculo
from .colecoes import ACAO_ENVIAR, ACAO_RECEBER
from .views.acao import Ui_Dialog


class Acao(subwindowsbase.Formulario):
    acao = None
    classe_ui = Ui_Dialog

    def accept(self):
        comprovante = self.ui.comprovante.text()
        data = qdate_datetime(self.ui.data.date())
        observacao = self.ui.observacao.toPlainText()
        with DataBase().obter_database().atomic():
            if self.acao == ACAO_ENVIAR:
                for av in self.instance:
                    av.ficha_enviada = True
                    av.data_enviada = data
                    av.forma_enviada = comprovante
            if self.acao == ACAO_RECEBER:
                for av in self.instance:
                    av.ficha_devolvida = True
                    av.data_devolvida = data
                    av.forma_devolvida = comprovante
            for av in self.instance:
                if av.observacao is None:
                    av.observacao = observacao
                else:
                    av.observacao += observacao
                av.save()
        return super().accept()

    def inicializar(self, *args, **kwargs):
        self.ui.comprovante.setValidator(TextoEmMaisculo(self))
        text_format_uppercase(self.ui.observacao)
        self.acao = kwargs.get('acao', None)
        data_limite = date(1900,1,1)
        if self.acao == ACAO_ENVIAR:
            self.setWindowTitle('Enviar formulário')
            self.ui.salvar.setText('Enviar')
        elif self.acao == ACAO_RECEBER:
            self.setWindowTitle('Receber formulário')
            self.ui.salvar.setText('Receber')
            for av in self.instance:
                if av.data_enviada > data_limite:
                    data_limite = av.data_enviada
            self.ui.data.setMinimumDate(datetime_qdate(data_limite))
        else:
            self.ui.salvar.setEnabled(False)
        
        data = date.today()
        if data > data_limite:
            self.ui.data.setDate(datetime_qdate(data))
        else:
            self.ui.data.setDate(datetime_qdate(data_limite))