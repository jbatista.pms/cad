from PySide2 import QtWidgets

from ComumComissoes.cargo import listar as car_listar, cadastro as car_cadastro
from ComumComissoes.lotacao import listar as lot_listar, cadastro as lot_cadastro
from QtUtils import QtUtils
from QtUtils.backup import backup
from QtUtils.mensageiro import Mensageiro
from QtUtils.registro.listar import ListarRegistro
from QtUtils.user import autenticacao, listar as user_listar, cadastro, usuario
from QtUtils.user.models import User

from avaliacao import listar as av_listar
from avaliacao.exportar import exportar_avaliacoes
from evento.exportar import exportar_eventos
from relatorios import RelatorioEstagiosCumpridos, RelatorioEstagiosInterrompidos
from servidor import listar as ser_listar, cadastro as ser_cadastro
from servidor.relatorio import ServidorRelatorioJanela
from situacao.cadastro import CadastroSituacao
from servidor.exportar import exportar_servidores
from situacao.listar import ListarSituacao
from .views import app


class JanelaPrincipal(QtWidgets.QMainWindow, Mensageiro):
    def __init__(self, parent=None):
        super(JanelaPrincipal, self).__init__(parent)
        self.ui = app.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.mdiArea.setActivationOrder(
            QtWidgets.QMdiArea.ActivationHistoryOrder
        )
        self.mdiArea = self.ui.mdiArea
        self.carregar_titulo()
        self.contruir()
    
    def alterar_nome(self):
        cadastro.AlterarNome(self, usuario.Usuario().getUsuario()).exec_()

    def alterar_senha(self):
        cadastro.AlterarSenha(self, usuario.Usuario().getUsuario()).exec_()

    def backup_db(self):
        backup.BackupDialogo(self).exec_()

    def cadastrar_cargo(self):
        car_cadastro.Cadastro(self).exec_()
    
    def cadastro_situacao(self):
        CadastroSituacao(self).exec_()
    
    def carregar_titulo(self):
        if not usuario.Usuario().getUsuario():
            self.ui.menuUsuarios.setEnabled(False)
            self.setWindowTitle(QtUtils.definicoes.nome_aplicacao)
        else:
            self.setWindowTitle(
                "%s -*-*-*- Usuário logado: %s" % (
                    QtUtils.definicoes.nome_aplicacao, usuario.Usuario().getUsuario().nome_acesso
                )
            )

    def configuracao(self):
        QtUtils.config.janela(parent=self).exec_()

    def exportar_avaliacoes(self):
        exportar_avaliacoes(self)

    def exportar_eventos(self):
        exportar_eventos(self)
    
    def exportar_servidores(self):
        exportar_servidores(self)

    def listar_cargos(self):
        car_listar.Control(self).show()

    def listar_lotacoes(self):
        lot_listar.Control(self).show()

    def listar_servidores(self):
        ser_listar.ServidoresListaJanela(self).show()

    def listar_avaliacoes(self):
        av_listar.Control(self).show()

    def nova_lotacao(self):
        lot_cadastro.Cadastro(self).exec_()

    def novo_servidor(self):
        ser_cadastro.Cadastro(self).exec_()
    
    def rlt_estagios_cumpridos(self):
        RelatorioEstagiosCumpridos(self).show()
    
    def rlt_estagios_interrompidos(self):
        RelatorioEstagiosInterrompidos(self).show()
    
    def rlt_servidores(self):
        ServidorRelatorioJanela(self).show()

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, User):
            if usuario.Usuario().getUsuario() is None:
                autenticacao.Autenticacao(self).exec_()
            elif instance == usuario.Usuario().getUsuario():
                usuario.Usuario().setUsuario(instance)
                self.carregar_titulo()

    def registros(self):
        ListarRegistro(self).show()
    
    def show(self) -> None:
        return super().showMaximized()
    
    def situacao(self):
        ListarSituacao(self).show()

    def usuarios(self):
        user_listar.Listar(self).show()
